<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "includes/learningcentre/header.php"; ?>
</head>
<body>
<header>
    <?php include "includes/learningcentre/navbar-lc.php"; ?>
</header>
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content"><!-- SLIDER BANNER-->
                <?php include "sections/learningcentre.php"; ?>
            </div>
        </div>
    </div>
</div>
<?php include "containts/learningcentre-modal.php"; ?>
<?php include "includes/learningcentre/footer.php"; ?>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/57f6eaec0188071f8b8e7ce8/1b2ak0q8h';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>