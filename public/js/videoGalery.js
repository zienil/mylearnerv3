// Functional for video galery
$(document).ready(function () {

// Modal Window for dynamically opening videos
    $('a[href^="https://www.youtube.com"]').on('click', function (e) {
        // Store the query string variables and values
        // Uses "jQuery Query Parser" plugin, to allow for various URL formats (could have extra parameters)
        var queryString = $(this).attr('href').slice($(this).attr('href').indexOf('?') + 1);
        var queryVars = $.parseQuery(queryString);
        var title = $(this).data("title");

        // if GET variable "v" exists. This is the Youtube Video ID
        if ('v' in queryVars) {
            // Prevent opening of external page
            e.preventDefault();
            var iFrameCode = '' +
                '<div class="embed-responsive embed-responsive-16by9">' +
                // '<iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=' + queryVars['v'] + '"></iframe>' +
                // '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' + queryVars['v'] + '"></iframe>' +
                '<object class="embed-responsive-item" data="https://www.youtube.com/embed/' + queryVars['v'] + '"></object>' +
                '</div>';

            // Replace Modal HTML with iFrame Embed
            $('#mediaModal .modal-body').html(iFrameCode);

            // Open Modal
            $('#mediaModal').modal();
            $('.modal-title').text(title);
        }
    });

// Clear modal contents on close.
// There was mention of videos that kept playing in the background.
    $('#mediaModal').on('hidden.bs.modal', function () {
        $('#mediaModal .modal-body').html('');
    });

});