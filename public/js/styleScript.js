// POPOVER HOVER FLOWCHART
$(document).on('ready', function () {
    $(".tutorial_popover").popover({
        trigger: 'manual',
        container: 'body',
        animate: false,
        html: true,
        // placement: function (tip, ele) {
        //     var width = $(window).width();
        //     return width >= 980 ? 'right' : ( width < 400 ? 'bottom' : 'bottom' );
        // },
        // placement: 'auto right',
        // placement: function (tip, ele) {
        //     var width = $(window).width();
        //     return width >= 980 ? "right" : ( width < 800 ? "bottom" : "bottom" );
        // }
        placement: function (context, source) {
            var position = $(source).position();

            // if (position.left > 980) {
            //     return "right";
            // }

            if (position.right < 800) {
                return "bottom";
            }
        },
        content: function () {
            // return $('#tutorial_preparation').html();
            // return $(this).data('popover-content');
            var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
            return clone;
        }
    }).on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(".popover").on("mouseleave", function () {
            $(_this).popover("hide");
        });
    }).on("mouseleave", function () {

        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
    });


    $(document).on("click", "#on9Submit-btn", function () {
        $('#f2fSubmit').collapse("hide");
        $('#f2fon9Submit').collapse("hide");
    });
    $(document).on("click", "#f2fSubmit-btn", function () {
        $('#on9Submit').collapse("hide");
        $('#f2fon9Submit').collapse("hide");
    });
    $(document).on("click", "#f2fon9Submit-btn", function () {
        $('#on9Submit').collapse("hide");
        $('#f2fSubmit').collapse("hide");
    });

    $(document).on("click", "#finalExamReason-btn", function () {
        $('#finalExamWOReason').collapse("hide");
    });
    $(document).on("click", "#finalExamWOReason-btn", function () {
        $('#finalExamReason').collapse("hide");
    });

    $(document).on("click", "#refer-btn", function () {
        $('#repeat').collapse("hide");
    });
    $(document).on("click", "#repeat-btn", function () {
        $('#refer').collapse("hide");
    });

    $(document).on("click", ".link", function (e) {
        e.preventDefault();
    });
});