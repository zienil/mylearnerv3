// owl carousel for ....
// var shw_slider_carousel = function() {
//     // owl carousel slider banner
//     $('.slider-banner').owlCarousel({
//         margin: 0,
//         loop: true,
//         //lazyLoad: true,
//         animateOut: 'fadeOut',
//         animateIn: 'fadeIn',
//         nav: false,
//         responsiveClass: true,
//         autoplay:true,
//         autoplayTimeout: 7000,
//         smartSpeed: 800,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             1024: {
//                 items: 1
//             }
//         }
//     });
//
// owl carousel event-detail-list-staff
$('.event-detail-list-staff').owlCarousel({
    margin: 30,
    loop: true,
    nav: false,
    responsiveClass: true,
    autoplay:true,
    autoplayTimeout: 7000,
    smartSpeed: 1000,
    responsive: {
        0: {
            items: 1
        },
        480: {
            items: 2
        },
        600: {
            items: 2
        },
        768: {
            items: 3,
            margin: 15
        },
        1024: {
            items: 3
        }
    }
});
//
//     // owl carousel top courses
//     $('.top-courses-slider').owlCarousel({
//         margin: 30,
//         loop: true,
//         nav: false,
//         responsiveClass: true,
//         smartSpeed: 1000,
//         responsive: {
//             0: {
//                 items: 1
//             },
//             1024: {
//                 items: 1
//             },
//             1025: {
//                 items: 2
//             }
//         }
//     });
//     // button click slider top courses
//     $('.group-btn-top-courses-slider .btn-prev').on('click', function(){
//         $('.top-courses-slider .owl-prev').click();
//     });
//     $('.group-btn-top-courses-slider .btn-next').on('click', function(){
//         $('.top-courses-slider .owl-next').click();
//     });
//
//     // owl carousel slider logos
//     $('.carousel-logos').owlCarousel({
//         margin: 115,
//         loop: true,
//         //lazyLoad: true,
//         nav: false,
//         autoplay:true,
//         autoplayTimeout: 5000,
//         smartSpeed: 1500,
//         responsiveClass: true,
//         responsive: {
//             0: {
//                 items: 2,
//                 margin: 30,
//             },
//             320: {
//                 items: 3,
//                 margin: 40,
//             },
//             480: {
//                 items: 3,
//                 margin: 50,
//             },
//             600: {
//                 items: 4,
//                 margin: 90,
//             },
//             768: {
//                 items: 5,
//                 margin: 60,
//             },
//             1024: {
//                 items: 5,
//                 margin: 90,
//             },
//             1025: {
//                 items: 6
//             }
//         }
//     });
//
// owl carousel slider best staff
$('.best-staff-content').owlCarousel({
    margin: 30,
    loop: true,
    nav: false,
    responsiveClass: true,
    //autoplay:true,
    autoplayTimeout: 5000,
    smartSpeed: 1000,
    responsive: {
        0: {
            items: 1,
            margin: 15,
        },
        400: {
            items: 2,
            margin: 15,
        },
        768: {
            items: 3
        },
        1024: {
            items: 3
        },
        1025: {
            items: 4
        }
    }
});
// button click slider best staff
$('.best-staff .group-btn-slider .btn-prev').on('click', function(){
    $('.best-staff .owl-prev').click();
});
$('.best-staff .group-btn-slider .btn-next').on('click', function(){
    $('.best-staff .owl-next').click();
});
//
//     // responsive for section pricing when screen < 768
//     if ($(window).width() <= 768){
//         $('.pricing-wrapper').owlCarousel({
//             margin: 15,
//             loop: true,
//             nav: false,
//             responsiveClass: true,
//             smartSpeed: 1000,
//             responsive: {
//                 0: {
//                     items: 1,
//                     margin: 0,
//                 },
//                 636: {
//                     items: 2
//                 },
//                 768: {
//                     items: 2
//                 }
//             }
//         });
//
//         $('.event-detail-content .row').owlCarousel({
//             margin: 15,
//             loop: true,
//             nav: false,
//             responsiveClass: true,
//             smartSpeed: 1000,
//             responsive: {
//                 0: {
//                     items: 1,
//                     margin: 0
//                 },
//                 768: {
//                     items: 2
//                 }
//             }
//         });
//     };
//     // button click slider
//     $('.pricing .group-btn-slider .btn-prev').on('click', function(){
//         $('.pricing-wrapper .owl-prev').click();
//     });
//     $('.pricing .group-btn-slider .btn-next').on('click', function(){
//         $('.pricing-wrapper .owl-next').click();
//     });
//
//     // Responsive for Progress bar when screen < 767
//     if ($(window).width() <= 767){
//         $('.progress-bar-wrapper .content').owlCarousel({
//             margin: 0,
//             loop: true,
//             nav: false,
//             responsiveClass: true,
//             smartSpeed: 1000,
//             responsive: {
//                 0: {
//                     items: 2,
//                     margin: 15,
//                 },
//                 480: {
//                     items: 2,
//                     margin: 15,
//                 },
//                 600: {
//                     items: 3
//                 },
//                 767: {
//                     items: 3
//                 }
//             }
//         });
//     };
//     // button click slider
//     $('.progress-bars .group-btn-slider .btn-prev').on('click', function(){
//         $('.progress-bars .owl-prev').click();
//     });
//     $('.progress-bars .group-btn-slider .btn-next').on('click', function(){
//         $('.progress-bars .owl-next').click();
//     });
// };