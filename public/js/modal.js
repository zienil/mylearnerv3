// FOR ALL MODAL POPOVER
$(document).ready(function () {
    $('.modalClick').on('click', function (e) {
        e.preventDefault();
        var title = $(this).data("title");
        var link = $(this).attr('href');

        // Replace Modal HTML with iFrame Embed
        $('#pageModal .modal-body').load(link);

        // Open Modal
        $('#pageModal').modal();
        $('.modal-title').text(title);
    });

    // Clear modal contents on close.
    // There was mention of videos that kept playing in the background.
    $('#pageModal').on('hidden.bs.modal', function () {
        $('#pageModal .modal-body').html('');
    });
});