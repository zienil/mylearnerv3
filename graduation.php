<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "includes/graduating/header.php"; ?>
</head>
<body>
<header>
    <?php include "includes/graduating/navbar-graduate.php"; ?>
</header>
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content"><!-- SLIDER BANNER-->
                <?php include "sections/graduation.php"; ?>
            </div>
        </div>
    </div>
</div>
<?php include "containts/modal-page.php"; ?>
<?php include "containts/popover-all.php"; ?>
<?php include "includes/graduating/footer.php"; ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/57f6eaec0188071f8b8e7ce8/1b2ak0q8h';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>