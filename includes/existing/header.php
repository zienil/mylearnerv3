<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>MyLearning Journey</title>

<!-- Bootstrap Core CSS -->
<link href="../public/plugins/bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">
<link href="../public/plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
<link href="../public/plugins/mybank/bankmy.css" rel="stylesheet">
<link href="../public/plugins/animate/animate.css" rel="stylesheet">
<link href="../public/plugins/hover/hover.css" rel="stylesheet">
<link href="../public/css/style.css" rel="stylesheet">
<link href="../public/css/addonStyle.css" rel="stylesheet">
<link href="../public/css/responsive.css" rel="stylesheet">
<link href="../public/css/btncolor.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->