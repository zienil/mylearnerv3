<div class="header-main homepage-01">
    <div class="container">
        <div class="header-main-wrapper">
            <div class="navbar-heade">
                <div class="logo pull-left"><a href="../index" class="header-logo"><img
                            src="../public/images/logo-color-4.png" alt=""/></a></div>
                <button type="button" data-toggle="collapse" data-target=".navigation"
                        class="navbar-toggle edugate-navbar"><span class="icon-bar"></span><span
                        class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <nav class="navigation collapse navbar-collapse pull-right">
                <ul class="nav-links nav navbar-nav">
                    <li><a href="../index" class="main-menu">Home</a></li>
                    <li class="dropdown active"><a href="#" class="main-menu">I Am A<span
                                class="fa fa-angle-down icons-dropdown"></span></a>
                        <ul class="dropdown-menu edugate-dropdown-menu-1">
                            <li class="active"><a href="../prospect.php" class="link-page">Prospective Student</a></li>
                            <li><a href="../newstudent" class="link-page">New Student</a></li>
                            <li><a href="../existing" class="link-page">Senior Student</a></li>
                            <li><a href="../graduation" class="link-page">Graduating Student</a></li>
                        </ul>
                    </li>
                    <li><a href="../learningvideos" class="main-menu">Learning Guide Video</a></li>
                    <li><a href="../learningcentre" class="main-menu">Learning Centres</a></li>
                    <li><a href="../faq" class="main-menu">FAQ</a></li>
                    <li><a href="../about" class="main-menu">Contact Us</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>