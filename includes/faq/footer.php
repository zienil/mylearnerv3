<footer>
    <div class="footer-main">
        <div class="container">
            <div class="hyperlink">
                <div class="pull-left hyper-left">
                    <ul class="list-inline">
                        <li>&copy; 2016-<?php echo date('Y'); ?></li>
                        <li><abbr title="Institute for Teaching and Learning Advancement"
                                  class="initialism">ITLA</abbr></li>
                        <li>All Right Reserved</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="../public/plugins/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../public/plugins/bootstrap-3.3.7/js/bootstrap.js"></script>
<script type="text/javascript" src="../public/plugins/jquery/SmoothScroll.js"></script>
<script type="text/javascript" src="../public/plugins/jquery/jquery.appear.js"></script>
<script type="text/javascript" src="../public/plugins/animate/wow.js"></script>
<script type="text/javascript" src="../public/js/main.js"></script>
<script type="text/javascript" src="../public/js/faq.js"></script>