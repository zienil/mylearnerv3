<!--ALL POPUP MODAL ON LEARNING CENTRE's LOCATION INSIDE LEARNING CENTRES PAGE-->

<!--NORTH-->
<div id="lc-kedahperlis" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="lc-kedahperlisLabel">Kedah & Perlis</h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-sg_petani" aria-controls="lc-sg_petani" role="tab" data-toggle="tab">Sungai Petani
                            Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-alorsetar" aria-controls="lc-alorsetar" role="tab" data-toggle="tab">Alor Setar
                            Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-sg_petani">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sg_petani.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-perak1">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/perak.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-alorsetar">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/alor_setar.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-penang" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="lc-penangLabel">Penang</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-seberangjaya" aria-controls="lc-seberangjaya" role="tab" data-toggle="tab">Seberang
                            Jaya Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-seberangjaya">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/penang.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-perak" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="lc-perakLabel">Perak</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-tanjongmalim" aria-controls="lc-tanjongmalim" role="tab" data-toggle="tab">Tanjong
                            Malim Learning</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-ipoh" aria-controls="lc-ipoh" role="tab" data-toggle="tab">Ipoh Learning
                            Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-greenhill" aria-controls="lc-greenhill" role="tab" data-toggle="tab">Greenhill
                            Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-manjung" aria-controls="lc-manjung" role="tab" data-toggle="tab">Manjung Learning
                            Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-tanjongmalim">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/tg_malim.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-ipoh">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/perak.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-greenhill">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/greenhill.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-manjung">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/manjung.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--EAST COST-->
<div id="lc-kelantan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Kelantan</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-kotabahru" aria-controls="lc-kotabahru" role="tab" data-toggle="tab">Kota Bharu
                            Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-kualakrai" aria-controls="lc-ipoh" role="tab" data-toggle="tab">Kuala Krai Learning
                            Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-kotabahru">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/kelantan.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-kualakrai">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/kuala_krai.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-terengganu" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terengganu</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-kterengganu" aria-controls="lc-kterengganu" role="tab" data-toggle="tab">Kuala
                            Terengganu Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-kterengganu">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/terengganu.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-pahang" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pahang</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-kuantan" aria-controls="lc-kuantan" role="tab" data-toggle="tab">Kuantan Learning
                            Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-temerloh" aria-controls="lc-temerloh" role="tab" data-toggle="tab">Temerloh
                            Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-kuantan">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/pahang.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-temerloh">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/temerloh.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--KLANG VALLEY-CENTRAL-->
<div id="lc-kl" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Kuala Lumpur</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-kl1" aria-controls="lc-kl1" role="tab" data-toggle="tab">Kuala Lumpur Learning
                            Centre (Postgraduate)</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-srirampai" aria-controls="lc-srirampai" role="tab" data-toggle="tab">Sri Rampai
                            Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-kl1">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/kl.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-srirampai">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sri_rampai.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-selangor" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Selangor</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-bangi" aria-controls="lc-bangi" role="tab" data-toggle="tab">Bangi Learning
                            Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-sa" aria-controls="lc-sa" role="tab" data-toggle="tab">Shah Alam Learning
                            Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-pj" aria-controls="lc-pj" role="tab" data-toggle="tab">Petaling Jaya Learning
                            Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-bangi">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/bangi.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-sa">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/shah_alam.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-pj">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/petaling_jaya.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--SOUTH-->
<div id="lc-n9" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Negeri Sembilan</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-sembilan" aria-controls="lc-sembilan" role="tab" data-toggle="tab">Seremban
                            Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-sembilan">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sembilan.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-melaka" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Melaka</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-melaka1" aria-controls="lc-melaka1" role="tab" data-toggle="tab">Melaka Learning
                            Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-melaka1">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/melaka.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-johor" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Johor</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-jb" aria-controls="lc-jb" role="tab" data-toggle="tab">Johor Bahru Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-pgudang" aria-controls="lc-pgudang" role="tab" data-toggle="tab">Pasir Gudang Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-pontian" aria-controls="lc-pontian" role="tab" data-toggle="tab">Pontian Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-bp" aria-controls="lc-bp" role="tab" data-toggle="tab">Batu Pahat Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-mersing" aria-controls="lc-mersing" role="tab" data-toggle="tab">Mersing Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-jb">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/johor.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-pgudang">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/pasir_gudang.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-pontian">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/pontian.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-bp">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/batu_pahat.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-mersing">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/mersing.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--SABAH AND SARAWAK-->
<div id="lc-sabah" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sabah</h4>
            </div>
            <div class="modal-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-kk" aria-controls="lc-kk" role="tab" data-toggle="tab">Kota Kinabalu Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-km" aria-controls="lc-km" role="tab" data-toggle="tab">Kota Marudu Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-sandakan" aria-controls="lc-sandakan" role="tab" data-toggle="tab">Sandakan Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-ld" aria-controls="lc-ld" role="tab" data-toggle="tab">Lahad Datu Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-tawau" aria-controls="lc-tawau" role="tab" data-toggle="tab">Tawau Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-keningau" aria-controls="lc-keningau" role="tab" data-toggle="tab">Keningau Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-labuan" aria-controls="lc-labuan" role="tab" data-toggle="tab">Labuan Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-kk">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sabah.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-km">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/kota_marudu.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-sandakan">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sandakan.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-ld">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/lahad_datu.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-tawau">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/tawau.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-keningau">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/keningau.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-labuan">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/labuan.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="lc-sarawak" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Sarawak</h4>
            </div>
            <div class="modal-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#lc-kucing" aria-controls="lc-kucing" role="tab" data-toggle="tab">Kuching Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-sibu" aria-controls="lc-sibu" role="tab" data-toggle="tab">Sibu Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-bintulu" aria-controls="lc-bintulu" role="tab" data-toggle="tab">Bintulu Learning Centre</a>
                    </li>
                    <li role="presentation">
                        <a href="#lc-miri" aria-controls="lc-miri" role="tab" data-toggle="tab">Miri Learning Centre</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="lc-kucing">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sarawak.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-sibu">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/sibu.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-bintulu">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/bintulu.html"></iframe>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="lc-miri">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="http://oum.edu.my/pages/prospective/prospective/LC/miri.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>