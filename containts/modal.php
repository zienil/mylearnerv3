<div id="oum" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">OUM HOME PAGE</h4>
            </div>
            <div class="modal-body">
                <div>
                    <a href="http://oum.edu.my" target="_blank">
                        <img src="../public/images/all/oum_home_page.png" class="img-thumbnail img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="offeredProg" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">PROGRAMMES OFFERED</h4>
            </div>
            <div class="modal-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#undergraduate" aria-controls="undergraduate" role="tab" data-toggle="tab">Undergraduate
                                Programs</a>
                        </li>
                        <li role="presentation">
                            <a href="#postgraduate" aria-controls="postgraduate" role="tab"
                               data-toggle="tab">Postgraduate Programs
                            </a>
                        </li>
<!--                        <li role="presentation">-->
<!--                            <a href="#online" aria-controls="online" role="tab" data-toggle="tab">-->
<!--                                Postgraduate Programs(100% On9)-->
<!--                            </a>-->
<!--                        </li>-->
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="undergraduate">
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF NURSING & HEALTH SCIENCE</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Bachelor of Nursing Science with Honours</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF EDUCATION AND LANGUAGE</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Diploma in Early Childhood Education</li>
                                                <li>Bachelor of Early Childhood Education with Honours</li>
                                                <li>Bachelor of Education (TESL) with Honours</li>
                                                <li>Bachelor of English Studies with Honours</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF APPLIED SOCIAL SCIENCES</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Diploma in Psychology</li>
                                                <li>Diploma in Islamic Studies with Education</li>
                                                <li>Diploma in Professional Communication</li>
                                                <li>Bachelor of Communication with Honours</li>
                                                <li>Bachelor of Islamic Studies (Islamic Management) with Honours</li>
                                                <li>Bachelor of Psychology with Honours</li>
                                                <li>Bachelor of Applied Social Science with Honours</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">OUM BUSINESS SCHOOL</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Executive Diploma in Logistics and Supply Chain Management</li>
                                                <li>Executive Diploma in Banking and Finance</li>
                                                <li>Executive Diploma in Management</li>
                                                <li>Diploma in Management</li>
                                                <li>Diploma in Human Resource Management</li>
                                                <li>Diploma in Accounting</li>
                                                <li>Bachelor of Accounting with Honours</li>
                                                <li>Bachelor of Business Administration with Honours</li>
                                                <li>Bachelor of Human Resource Management with Honours</li>
                                                <li>Bachelor of Management with Honours</li>
                                                <li>Bachelor of Tourism Management with Honours</li>
                                                <li>Bachelor of Banking and Finance with Honours</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF SCIENCE & TECHNOLOGY</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Executive Diploma in Occupational Safety And Health</li>
                                                <li>Bachelor of Digital Media Design with Honours</li>
                                                <li>Bachelor of Information Technology with Honours</li>
                                                <li>Bachelor of Manufacturing Management with Honours</li>
                                                <li>Bachelor of Occupational Health and Safety Management with Honours</li>
                                                <li>Bachelor of Science in Project and Facility Management with Honours</li>
                                                <li>Bachelor of Environmental Sustainability Management with Honours</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="postgraduate">
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF NURSING & HEALTH SCIENCE</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Master of Nursing</li>
                                                <li>Doctor of Nursing</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF APPLIED SOCIAL SCIENCES</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Master of Islamic Studies</li>
                                                <li>Master of Corporate Communication (MCC)</li>
                                                <li>Master of Counselling</li>
                                                <li>Master of Psychology</li>
                                                <li>Doctor of Philosophy (Art)</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF EDUCATION AND LANGUAGE</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Postgraduate Diploma in Teaching</li>
                                                <li>Master of Instructional Design and Technology</li>
                                                <li>Master of Education</li>
                                                <li>Master of English Studies</li>
                                                <li>Master in Early Childhood Education (MECHE)</li>
                                                <li>Doctor of Philosophy (Education)</li>
                                                <li>Doctor of Education</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">FACULTY OF SCIENCE & TECHNOLOGY</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Master of Project Management</li>
                                                <li>Master of Occupational Safety and Health Risk Management</li>
                                                <li>Master of Quality Management</li>
                                                <li>Master of Facility Management</li>
                                                <li>Doctor of Philosophy (Information Technology)</li>
                                                <li>Doctor of Philosophy (Science)</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">OUM BUSINESS SCHOOL</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ol>
                                                <li>Master of Business Administration</li>
                                                <li>Master of Business Administration - Fully Online</li>
                                                <li>Master of Management</li>
                                                <li>Master of Management - Fully Online</li>
                                                <li>Master of Human Resource Management</li>
                                                <li>Doctor of Philosophy (Business Administration)</li>
                                                <li>Doctor of Philosophy (Business Administration) - Fully Online</li>
                                                <li>Doctor of Business Administration</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <div role="tabpanel" class="tab-pane" id="online">-->
<!--                            <br/>-->
<!--                            <div class="panel panel-info">-->
<!--                                <div class="panel-heading">-->
<!--                                    <h3 class="panel-title">Postgraduate Programs(100% Online)</h3>-->
<!--                                </div>-->
<!--                                <div class="panel-body">-->
<!--                                    <ol>-->
<!--                                        <li>Master of Business Administration (Fully Online)</li>-->
<!--                                        <li>Master of Management (Fully Online)</li>-->
<!--                                        <li>Doctor of Philosophy (Business Administration) (Fully Online)</li>-->
<!--                                    </ol>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="on9Application" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">ONLINE APPLICATION</h4>
            </div>
            <div class="modal-body">
                <div>
                    <a href="http://oum.edu.my" target="_blank">
                        <img src="../public/images/myvle/announcement.png"
                             class="img-thumbnail img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="AoA" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">ACKNOWLEDGEMENT OF ACCEPTANCE</h4>
            </div>
            <div class="modal-body">
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav  nav-pills nav-justified" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">Make Payment</a>
                        </li>
                        <li role="presentation">
                            <a href="#aoto" aria-controls="aoto" role="tab" data-toggle="tab">
                                Accept Offer Through Online
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="payment">
                            <br/>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-money"></i> Cash Payment</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <i class="myicon myicon-bimb besar-3x"></i>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading text-dark">Bank Islam Malaysia Berhad</h4>
                                            <ul class="text-primary">
                                                <li>Bank Islam Malaysia Berhad</li>
                                                <li>Payment made to UNITEM</li>
                                                <li>Account No: 14041-01-0053385</li>
                                                <li>Payment slip can be obtained from Bank Islam branches</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <img class="media-object img-thumbnail img-responsive"
                                                 src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTM0YjhmZjRkOCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MzRiOGZmNGQ4Ij48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQzLjUiIHk9Ijc0LjgiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                 title="BSN">
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading text-dark">GIRO Bank Simpanan Nasional</h4>
                                            <ul class="text-primary">
                                                <li>GIRO Bank Simpanan Nasional</li>
                                                <li>Payment made to UNIVERSITI TERBUKA MALAYSIA</li>
                                                <li>Account No: 14100-29-00022495-2</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-bank"></i> Internet Banking</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-right">
                                            <a href="https://www.bankislam.biz/" target="_blank">
                                                <i class="myicon myicon-bimb besar-3x"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-left">
                                            <a href="http://cimbclicks.com.my/" target="_blank">
                                                <i class="myicon myicon-cimb besar-3x"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-credit-card"></i> Credit Card</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-right">
                                            <i class="fa fa-4x fa-cc-mastercard text-danger"></i>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-left">
                                            <i class="fa fa-4x fa-cc-visa text-warning"></i>
                                        </div>
                                    </div>
                                    <p class="text-center text-primary">OUM Main Campus & PPW Center Only</p>
                                </div>
                            </div>
                            <br/>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-envelope-o"></i> Cheque/Money Order/Postal
                                        Order</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-md-offset-5">
                                            <i class="fa fa-3x fa-envelope text-dark"></i>
                                        </div>
                                    </div>
                                    <p class="text-center text-primary">Payee: UNIVERSITI TERBUKA MALAYSIA</p>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="aoto">
                            <br/>
                            <div>
                                <a href="http://fastrac.oum.edu.my" target="_blank">
                                    <img src="../public/images/all/letterOfAdminsion.png"
                                         class="img-thumbnail img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="registration_kit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">REGISTRATION KIT</h4>
            </div>
            <div class="modal-body">
                <div>
                    <img src="../public/images/all/importantDocs.png" class="img-thumbnail img-responsive">
                </div>
                <br/>
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills nav-justified" role="tablist">
                        <li role="presentation">
                            <a href="#lod" aria-controls="lod" role="tab" data-toggle="tab">Letter of
                                Admission</a>
                        </li>
                        <li role="presentation">
                            <a href="#rg" aria-controls="rg" role="tab" data-toggle="tab">
                                Registration Guide
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#rf" aria-controls="rf" role="tab" data-toggle="tab">
                                Registration Form
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#ds" aria-controls="ds" role="tab" data-toggle="tab">
                                Declaration Sheet
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="lod">
                            <br/>
                            <div>
                                <img src="../public/images/all/letterOfAdminsion.png"
                                     class="img-thumbnail img-responsive">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="rg">
                            <br/>
                            <div>
                                <img src="../public/images/all/registration_guide.png"
                                     class="img-thumbnail img-responsive">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="rf">
                            <br/>
                            <div>
                                <img src="../public/images/all/registration_form.png"
                                     class="img-thumbnail img-responsive">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="ds">
                            <br/>
                            <div>
                                <img src="../public/images/all/declarationSheet.png"
                                     class="img-thumbnail img-responsive">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="bkb" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">BENGKEL KEMAHIRAN BELAJAR (myVLE)</h4>
            </div>
            <div class="modal-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#1" aria-controls="1" role="tab" data-toggle="tab">Main Page</a>
                        </li>
                        <li role="presentation">
                            <a href="#2" aria-controls="2" role="tab" data-toggle="tab">myCourse</a>
                        </li>
                        <li role="presentation">
                            <a href="#3" aria-controls="3" role="tab" data-toggle="tab">eForum</a>
                        </li>
                        <li role="presentation">
                            <a href="#4" aria-controls="4" role="tab" data-toggle="tab">OUM Doc</a>
                        </li>
                        <li role="presentation">
                            <a href="#5" aria-controls="5" role="tab" data-toggle="tab">Resource</a>
                        </li>
                        <li role="presentation">
                            <a href="#6" aria-controls="6" role="tab" data-toggle="tab">Digital Library</a>
                        </li>
                        <li role="presentation">
                            <a href="#7" aria-controls="7" role="tab" data-toggle="tab">eService</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="1">
                            <br/>
                            <img src="../public/images/myvle/mainPage.png" alt="MyVLE Main Page"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="2">
                            <br/>
                            <img src="../public/images/myvle/mycourse.png" alt="List of Courses"
                                 class="img-thumbnail img-responsive"/>
                            <br/>
                            <img src="../public/images/myvle/mycourse1.png" alt="myCourse Page"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="3">
                            <br/>
                            <img src="../public/images/myvle/eforum.png" alt="eForum"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="4">
                            <br/>
                            <img src="../public/images/myvle/oumDocs.png" alt="OUM Doc"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="5">
                            <br/>
                            <img src="../public/images/myvle/oumResource.png" alt="Resource"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="6">
                            <br/>
                            <img src="../public/images/myvle/digitalLibrary.png" alt="Digital Library"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="7">
                            <br/>
                            <section class="historySection">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel-group myAccordion" id="accordionEservice" role="tablist">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOneEservice">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                                           href="#collapseOneEservice"
                                                           aria-expanded="true" aria-controls="collapseOneEservice">
                                                            ONLINE REGISTRATION
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOneEservice" class="panel-collapse collapse in"
                                                     role="tabpanel"
                                                     aria-labelledby="headingOneEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">MyVLE</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">eService</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">UKR Online</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">Online Registration</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">Choose Subject for Next
                                                                        Semester</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">Preview & Print Slip UKR03</a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwoEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseTwoEservice"
                                                           aria-expanded="false" aria-controls="collapseTwoEservice">
                                                            STATUS OF APPLICATIONS
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwoEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingTwoEservice">
                                                    <div class="panel-body">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThreeEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseThreeEservice" aria-expanded="false"
                                                           aria-controls="collapseThreeEservice">
                                                            TRANSFER CREDIT (UKR04)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThreeEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingThreeEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">MyVLE</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">eService</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">UKR Online</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">Transfer Credit</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block disabled"
                                                                       role="button">
                                                                        Choose Subject for transfer<br/>credit (max 10
                                                                        subjects)
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>Records
                                                                        and Operations Department
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFourEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseFourEservice" aria-expanded="false"
                                                           aria-controls="collapseFourEservice">
                                                            ADD/DROP COURSE (UKR06)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFourEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingFourEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Transfer Credit
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Choose Subject for transfer credit<br/>(max 10
                                                                        subjects)
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>Records
                                                                        and Operations Department
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFiveEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseFiveEservice" aria-expanded="false"
                                                           aria-controls="collapseFiveEservice">
                                                            WITHDRAW SUBJECT (UKR07)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFiveEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingFiveEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Withdraw Subject
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Choose Subject to Withdraw
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>Records
                                                                        and Operations Department
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSixEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseSixEservice" aria-expanded="false"
                                                           aria-controls="collapseSixEservice">
                                                            CHANGE PROGRAMME (UKR08)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSixEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingSixEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Ukr Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Change Programme
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Choose New Programme
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>Faculty
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSevenEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseSevenEservice" aria-expanded="false"
                                                           aria-controls="collapseSevenEservice">
                                                            DEFERMENT SEMESTER (UKR10)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSevenEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingSevenEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Deferment Semester
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Fill Up Deferment Application
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>
                                                                        Learning Center
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingEightEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseEightEservice" aria-expanded="false"
                                                           aria-controls="collapseEightEservice">
                                                            WITHDRAW SUBJECT a.k.a QUIT STUDIES (UKR11)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseEightEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingEightEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Withdraw/Quit
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Fill up Withdraw/Quit application
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>
                                                                        Learning Center
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingNineEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseNineEservice" aria-expanded="false"
                                                           aria-controls="collapseNineEservice">
                                                            CHANGE LEARNING CENTER (UKR12)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseNineEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingNineEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Change Learning Center
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Reason for Changing Learning Center
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>
                                                                        Learning Center
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTenEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseTenEservice" aria-expanded="false"
                                                           aria-controls="collapseTenEservice">
                                                            ONLINE LEARNING (OL) (UKR16)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTenEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingTenEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Online Learning (OL)
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Choose subject for online learning
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>
                                                                        Record and Operations Department
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingElevenEservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapseElevenEservice" aria-expanded="false"
                                                           aria-controls="collapseElevenEservice">
                                                            DISCOUNT ON STUDY FEES (OKU / SENIOR CITIZEN)(UKR18)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseElevenEservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="headingElevenEservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        MyVLE
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        eService
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        UKR Online
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        On Study Fees (OKU / Senior Citizen)
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Identification Card No. for the<br/>physically
                                                                        disabled & type of disability
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Preview & Submit Application Form to<br/>Record
                                                                        and Operations Department
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12Eservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapse12Eservice" aria-expanded="false"
                                                           aria-controls="collapse12Eservice">
                                                            ACTIVATION (UKR17)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse12Eservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="heading12Eservice">
                                                    <div class="panel-body">
                                                        <div class="col-lg-10 col-md-offset-4">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Visit Learning Center
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        Fill Up form UKR17(Manual)
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5 text-center marging-arrow">
                                                                    <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <a href="#"
                                                                       class="btn btn-success btn-block center-block disabled"
                                                                       role="button">
                                                                        LC will submit Application Form<br/>to Records
                                                                        and Operations Department
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13Eservice">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" data-toggle="collapse"
                                                           data-parent="#accordionEservice"
                                                           href="#collapse13Eservice" aria-expanded="false"
                                                           aria-controls="collapse13Eservice">
                                                            COURSE EXEMPTION (UKR21)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse13Eservice" class="panel-collapse collapse"
                                                     role="tabpanel"
                                                     aria-labelledby="heading13Eservice">
                                                    <div class="panel-body">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myvleModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">FIRST TIME LOGIN (MyVLE)</h4>
            </div>
            <div class="modal-body">
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#1ftl" aria-controls="1" role="tab" data-toggle="tab">Login Page</a>
                        </li>
                        <li role="presentation">
                            <a href="#2ftl" aria-controls="2" role="tab" data-toggle="tab">First Time Login</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="1ftl">
                            <br/>
                            <img src="../public/images/myvle/oumvle.png" alt="MyVLE Login Page"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="2ftl">
                            <br/>
                            <img src="../public/images/myvle/firstimeUsername.png" alt="First Time Login"
                                 class="img-thumbnail img-responsive"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="paymentModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">PAYMENT</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money"></i> Cash Payment</h3>
                    </div>
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="myicon myicon-bimb besar-3x"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading text-dark">Bank Islam Malaysia Berhad</h4>
                                <ul class="text-primary">
                                    <li>Bank Islam Malaysia Berhad</li>
                                    <li>Payment made to UNITEM</li>
                                    <li>Account No: 14041-01-0053385</li>
                                    <li>Payment slip can be obtained from Bank Islam branches</li>
                                </ul>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left media-middle">
                                <img class="media-object img-thumbnail img-responsive"
                                     src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgdmlld0JveD0iMCAwIDE0MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzE0MHgxNDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTM0YjhmZjRkOCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MzRiOGZmNGQ4Ij48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9IjQzLjUiIHk9Ijc0LjgiPjE0MHgxNDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                     title="BSN">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading text-dark">GIRO Bank Simpanan Nasional</h4>
                                <ul class="text-primary">
                                    <li>GIRO Bank Simpanan Nasional</li>
                                    <li>Payment made to UNIVERSITI TERBUKA MALAYSIA</li>
                                    <li>Account No: 14100-29-00022495-2</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bank"></i> Internet Banking</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-right">
                                <a href="https://www.bankislam.biz/" target="_blank">
                                    <i class="myicon myicon-bimb besar-3x"></i>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-left">
                                <a href="http://cimbclicks.com.my/" target="_blank">
                                    <i class="myicon myicon-cimb besar-3x"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-credit-card"></i> Credit Card</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-right">
                                <i class="fa fa-4x fa-cc-mastercard text-danger"></i>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 text-center pull-left">
                                <i class="fa fa-4x fa-cc-visa text-warning"></i>
                            </div>
                        </div>
                        <p class="text-center text-primary">OUM Main Campus & PPW Center Only</p>
                    </div>
                </div>
                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-envelope-o"></i> Cheque/Money Order/Postal Order</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-md-offset-5">
                                <i class="fa fa-3x fa-envelope text-dark"></i>
                            </div>
                        </div>
                        <p class="text-center text-primary">Payee: UNIVERSITI TERBUKA MALAYSIA</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="session_register" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">FORMS TO BE COMPLETE</h4>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item active"><i class="fa fa-hashtag"></i> Forms to be
                        complete
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i> Proof of
                        payment
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i>
                        Acknowledgement of Acceptance (UKR02)
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i> Course
                        Registration Form (UKR03)
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i> Personal
                        biodata form (UKR 13)
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i> Letter
                        of confirmation from employer
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i> Copy of
                        Letter of Offer
                    </li>
                    <li class="list-group-item"><i class="fa fa-check text-danger"></i> Photo
                        (2)
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="dlAssgQ" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">DOWNLOAD ASSIGNMENT
                    <small>Step by step process</small>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">MyVLE</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">myCourse - Choose
                            Subject</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Assignment - Choose
                            Semester</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Question</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="AssSubmit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">ASSIGNMENT SUBMISSION</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Submission Modes</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-md-5 text-center marging-arrow">
                        <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                    </div>
                    <div class="col-xs-4 col-md-2 text-center"></div>
                    <div class="col-xs-4 col-md-5 text-center marging-arrow">
                        <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-md-4 margin-list-group">
                        <div class="list-group">
                            <button id="on9Submit-btn" type="button" class="list-group-item list-group-item-info"
                                    data-toggle="collapse" data-target="#on9Submit" aria-expanded="false"
                                    aria-controls="on9Submit" style="text-align: center;">
                                Online Submission
                            </button>
                        </div>
                    </div>
                    <div class="col-xs-8 col-md-8 margin-list-group">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Face 2 Face Submission</a>
                        <div class="row">
                            <div class="col-xs-4 col-md-5 text-center marging-arrow">
                                <i class="fa fa-2x fa-long-arrow-down"></i>
                            </div>
                            <div class="col-xs-4 col-md-2 text-center"></div>
                            <div class="col-xs-4 col-md-5 text-center marging-arrow">
                                <i class="fa fa-2x fa-long-arrow-down"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6 text-center">
                                <div class="list-group">
                                    <button id="f2fSubmit-btn" type="button"
                                            class="list-group-item list-group-item-info"
                                            data-toggle="collapse" data-target="#f2fSubmit" aria-expanded="false"
                                            aria-controls="f2fSubmit" style="text-align: center;">
                                        Face 2 Face Learners
                                    </button>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <div class="list-group">
                                    <button id="f2fon9Submit-btn" type="button"
                                            class="list-group-item list-group-item-info"
                                            data-toggle="collapse" data-target="#f2fon9Submit" aria-expanded="false"
                                            aria-controls="f2fon9Submit" style="text-align: center;">
                                        Online Learners
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="collapse" id="on9Submit">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Online Submission</h2>
                        </div>
                        <div class="panel-body panel-content">
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">MyVLE</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">myCourse - Choose Subject</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Assignment - Choose Semester</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Submission</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Choose file to upload</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Submit assignment</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="f2fSubmit">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Face 2 Face Submission</h2>
                        </div>
                        <div class="panel-body panel-content">
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Format Checking</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Print Final Copy</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Submit to Face 2 Face Tutor</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Assignment Submission Receipt</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="f2fon9Submit">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">Face 2 Face Submission (Online Learners)</h2>
                        </div>
                        <div class="panel-body panel-content">
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Format Checking</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Print Final Copy</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-10 col-md-offset-1">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Refer faculty
                                        announcement for<br/>submission mode</a>
                                </div>
                            </div>
                            <div class="col-md-12 text-center marging-arrow">
                                <i class="fa fa-2x fa-angle-double-down"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-8 col-md-offset-2">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Assignment
                                        Submission Receipt</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="chkExamTT" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CHECK EXAM TIMETABLE
                    <small>Step by step process</small>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">MyVLE</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">myProfile</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Examination</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">myExamination Timetable</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="printExamSlip" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">PRINT EXAM SLIP
                    <small>Step by step process</small>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">MyVLE</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">myProfile</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Examination</a>
                    </div>
                </div>
                <div class="col-md-12 text-center marging-arrow">
                    <i class="fa fa-2x fa-angle-double-down"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Print Exam Result Slip</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="finalExamReason" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">FINAL EXAM WITH REASON</h4>
            </div>
            <div class="modal-body">
                <img src="/public/images/all/examWithReason.png" class="img-thumbnail img-responsive">
            </div>
        </div>
    </div>
</div>

<div id="finalExamWOReason" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">FINAL EXAM WITHOUT REASON</h4>
            </div>
            <div class="modal-body">
                <img src="/public/images/all/examWithOutReason.png" class="img-thumbnail img-responsive">
            </div>
        </div>
    </div>
</div>

<div id="result1" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RESULT</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Grade</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-md-4 text-center marging-arrow">
                        <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                    </div>
                    <div class="col-xs-4 col-md-4 text-center marging-arrow">
                        <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                    </div>
                    <div class="col-xs-4 col-md-4 text-center marging-arrow">
                        <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 col-md-4 margin-list-group">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Pass</a>
                        <div class="row">
                            <div class="col-xs-8 col-md-8 text-center marging-arrow">
                                <i class="fa fa-2x fa-long-arrow-down"></i>
                            </div>
                        </div>
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Preparation for next<br/>semester</a>
                    </div>
                    <div class="col-xs-4 col-md-4 margin-list-group">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Fail</a>
                        <div class="row">
                            <div class="col-xs-4 col-md-5 text-center marging-arrow">
                                <i class="fa fa-2x fa-long-arrow-down"></i>
                            </div>
                            <div class="col-xs-4 col-md-2 text-center"></div>
                            <div class="col-xs-4 col-md-5 text-center marging-arrow">
                                <i class="fa fa-2x fa-long-arrow-down"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-md-6 text-center">
                                <div class="list-group">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Refer</a>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <div class="list-group">
                                    <a href="#" class="btn btn-success btn-block disabled" role="button">Repeat</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-md-4 margin-list-group">
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Incomplete</a>
                        <div class="row">
                            <div class="col-xs-8 col-md-8 text-center marging-arrow">
                                <i class="fa fa-2x fa-long-arrow-down"></i>
                            </div>
                        </div>
                        <a href="#" class="btn btn-success btn-block disabled" role="button">Register for final<br/>exam (within 1 year)</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>