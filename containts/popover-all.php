<!--PROSPECT-->
<div id="welcome" class="hide">
    <div class="col-lg-12 col-sm-6">
        <div class="row">
            <div class="col-lg-12">
                <a href="http://www.oum.edu.my" target="_blank" class="btn btn-primary btn-block hvr-buzz-out">
                    OUM Page
                </a>
            </div>
        </div>
        <br/>
    </div>
</div>

<div id="progOff" class="hide">
    <div class="col-lg-12 col-sm-6">
        <div class="row">
            <div class="col-lg-12">
                <a href="../modal/prospect2.php" class="btn btn-primary btn-block modalClick hvr-buzz-out pull-left"
                   data-title="PROGRAMMES OFFERED">Programmes Offered in OUM</a>
            </div>
        </div>
        <br/>
    </div>
</div>

<div id="on9Apps" class="hide">
    <div class="col-lg-12 col-sm-6">
        <div class="row">
            <div class="col-lg-12">
                <a href="http://fastrac.oum.edu.my/" target="_blank" class="btn btn-primary btn-block hvr-buzz-out">Welcome
                    Aboard</a>
            </div>
        </div>
        <br/>
    </div>
</div>

<!--NEW STUDENTS + EXISTING-->
<div id="registration" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent1.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="ACKNOWLEDGEMENT OF ACCEPTANCE">Acknowledgement of Acceptance</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent2.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="REGISTRATION KIT">Registration Kit</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent3.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="MAKE PAYMENT">Make Payment</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Collect Material</a>
        </div>
    </div>
    <br/>
</div>

<div id="myvle" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="http://www.oum.edu.my" target="_blank" class="btn btn-primary btn-block hvr-float-shadow"
               role="button">
                OUM Website<br/>(www.oum.edu.my)
            </a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button"> Student/Tutor</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button"> First Time Login<br/>(Pre-Registered/Registered)</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Enter IC Number</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Enter Personal
                Information<br/>SUBMIT</a>
        </div>
    </div>
    <br/>
</div>

<div id="eservice" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent4.php" class="btn btn-primary btn-block modalClick hvr-buzz-out"
               data-title="OUM Virtual Learning Environment">OUM Virtual Learning Environment</a>
        </div>
    </div>
    <br/>
</div>

<div id="tutorial_preparation" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent11.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="Retrieve Academic Calendar">Retrieve Academic Calendar</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent12.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="Check Class Timetable">Check Class Timetable</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Read Module</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Class Started</a>
        </div>
    </div>
    <br/>
</div>

<div id="tutorial_I" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Attend Class</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Ice-breaking<br/>– Tutor / Classmates</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Signed Attendance</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button"> Course Overview</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Assignment Discussion</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Preparation<br/>for Next
                Tutorial</a>
        </div>
    </div>
    </br>
</div>

<div id="eforum" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">OUM Portal</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">myInspire</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Choose Subject</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">View eForum</a>
        </div>
    </div>
    <br/>
</div>

<div id="tutorial_II" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Attend Class</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Signed Attendance</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Course Discussion</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">
                Preparation<br/>for Next Tutorial</a>
        </div>
    </div>
    <br/>
</div>

<div id="assignment_preparation" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent5.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="DOWNLOAD ASSIGNMENT">Download<br/>Assignment Question</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">
                Assignment discussion<br/>with Tutor/e-Tutor</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Assignment Tips</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../learningvideos" class="btn btn-primary btn-block learningVideoAG hvr-float-shadow"
               data-filter=".ag">Draft
                your assignment</a>
        </div>
    </div>
    <br/>
</div>

<div id="tutorial_III" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Attend Class</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Signed Attendance</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button"> Course Discussion</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Preparation for Next Tutorial</a>
        </div>
    </div>
    <br/>
</div>

<div id="assg_submit" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent6.php" class="btn btn-primary btn-block modalClick hvr-buzz-out"
               data-title="ASSIGNMENT SUBMISSION">Assignment Submission</a>
            <br/>
        </div>
    </div>
</div>

<div id="tutorial_IV" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Attend Class</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Signed Attendance</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Course Discussion</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Assignment Submission<br/>Face 2 Face &
                Online</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Preparation for Final Exam</a>
        </div>
    </div>
    <br/>
</div>

<div id="exam_preparation" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent7.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="CHECK EXAM TIMETABLE">Check Exam Time Table</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent8.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="PRINT EXAM SLIP">Print Exam Slip</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Confirm Exam Location</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent14.php" class="btn btn-primary btn-block modalClick hvr-float-shadow" role="button">Read Exam Rules &
                Regulation</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Matric Card/<br/>Identity Card
                /Stationery</a>
        </div>
    </div>
    <br/>
</div>

<div id="final_exam" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent9.php" class="btn btn-primary btn-block modalClick hvr-buzz-out"
               data-title="Final Exam">Final Exam</a>
        </div>
    </div>
</div>

<div id="result" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent10.php" class="btn btn-primary btn-block modalClick hvr-buzz-out"
               data-title="EXAM RESULT">Exam Result</a>
        </div>
    </div>
    <br/>
</div>

<div id="assign_result" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent15.php" class="btn btn-primary btn-block modalClick hvr-buzz-out"
               data-title="ASSIGNMENT RESULT">Assign Result</a>
        </div>
    </div>
    <br/>
</div>

<div id="pre_registration" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">OUM Portal</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">eServices</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">UKR Online</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Online Registration</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Choose Subject<br/>for Next Semester</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Choose Timetable</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Preview & Print Slip UKR03</a>
        </div>
    </div>
    <br/>
</div>


<!--GRADUATING-->
<div id="registration1" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/graduate1.php" class="btn btn-primary btn-block modalClick  hvr-float-shadow"
               data-title="OUM PORTAL ANNOUNCEMENT">Announcements - OUM Portal</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Registration at Learning Centre</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/newstudent3.php" class="btn btn-primary btn-block modalClick hvr-float-shadow"
               data-title="MAKE PAYMENT">Make Payment</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="#" class="btn btn-info-1 btn-block disabled" role="button">Collect Material</a>
        </div>
    </div>
    <br/>
</div>

<div id="graduation" class="hide">
    <div class="row">
        <div class="col-lg-12">
            <a href="../modal/graduate2.php" class="btn btn-primary btn-block modalClick hvr-buzz-out"
               data-title="GRADUATION">Graduation</a>
        </div>
    </div>
</div>

