<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active">
            <a href="#1" aria-controls="1" role="tab" data-toggle="tab">Main Page</a>
        </li>
        <li role="presentation">
            <a href="#2" aria-controls="2" role="tab" data-toggle="tab">myInspire</a>
        </li>
        <li role="presentation">
            <a href="#4" aria-controls="4" role="tab" data-toggle="tab">OUM Doc</a>
        </li>
        <li role="presentation">
            <a href="#5" aria-controls="5" role="tab" data-toggle="tab">Resource</a>
        </li>
        <li role="presentation">
            <a href="#6" aria-controls="6" role="tab" data-toggle="tab">Digital Library</a>
        </li>
        <li role="presentation">
            <a href="#7" aria-controls="7" role="tab" data-toggle="tab">eService</a>
        </li>
        <li role="presentation">
            <a href="#8" aria-controls="8" role="tab" data-toggle="tab">eCRM</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="1">
            <br/>
            <img src="../public/images/myvle/mainPage.png" alt="OUM Portal Main Page"
                 class="img-thumbnail img-responsive"/>
        </div>
        <div role="tabpanel" class="tab-pane" id="2">
            <br/>
            <div class="embed-responsive embed-responsive-16by9">
                <object class="embed-responsive-item" data="https://www.youtube.com/embed/DBeKxxR5RY0"></object>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="4">
            <br/>
            <img src="../public/images/myvle/oumDocs.png" alt="OUM Doc" class="img-thumbnail img-responsive"/>
        </div>
        <div role="tabpanel" class="tab-pane" id="5">
            <br/>
            <img src="../public/images/myvle/oumResource.png" alt="Resource"
                 class="img-thumbnail img-responsive"/>
        </div>
        <div role="tabpanel" class="tab-pane" id="6">
            <br/>
            <img src="../public/images/myvle/digitalLibrary.png" alt="Digital Library"
                 class="img-thumbnail img-responsive"/>
        </div>
        <div role="tabpanel" class="tab-pane" id="7">
            <br/>
            <section class="historySection">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-group myAccordion" id="accordionEservice" role="tablist">
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingOneEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseOneEservice" aria-expanded="true"
                                           aria-controls="collapseOneEservice" class="accordion-toggle">
                                            ONLINE REGISTRATION
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOneEservice" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingOneEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">OUM Portal</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">eService</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn"><span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">UKR Online</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn"><span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">Online Registration</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn"><span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">Choose Subject for Next
                                                        Semester</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn"><span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">Choose Timetable</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn"><span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">Preview & Print Slip UKR03</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingThreeEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseThreeEservice" aria-expanded="false"
                                           aria-controls="collapseThreeEservice" class="accordion-toggle">
                                            TRANSFER CREDIT (UKR04)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThreeEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingThreeEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">OUM Portal</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">eService</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">UKR Online</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">Transfer Credit</a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#" class="btn btn-success-1 btn-block disabled"
                                                       role="button">
                                                        Choose Subject for transfer<br/>credit (max 10
                                                        subjects)
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>Records
                                                        and Operations Department
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingFourEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseFourEservice" aria-expanded="false"
                                           aria-controls="collapseFourEservice" class="accordion-toggle">
                                            ADD/DROP COURSE (UKR06)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFourEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingFourEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Add/Drop courses
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Choose Subject for adding<br/>& dropping courses
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>Records
                                                        and Operations Department
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingFiveEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseFiveEservice" aria-expanded="false"
                                           aria-controls="collapseFiveEservice" class="accordion-toggle">
                                            WITHDRAW SUBJECT (UKR07)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFiveEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingFiveEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Withdraw Subject
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Choose Subject to Withdraw
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>Records
                                                        and Operations Department
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingSixEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseSixEservice" aria-expanded="false"
                                           aria-controls="collapseSixEservice" class="accordion-toggle">
                                            CHANGE PROGRAMME (UKR08)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSixEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingSixEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Ukr Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Change Programme
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Choose New Programme
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>Faculty
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingSevenEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseSevenEservice" aria-expanded="false"
                                           aria-controls="collapseSevenEservice"
                                           class="accordion-toggle">
                                            DEFERMENT SEMESTER (UKR10)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseSevenEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingSevenEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Deferment Semester
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Fill Up Deferment Application
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>
                                                        Learning Centre
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingEightEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseEightEservice" aria-expanded="false"
                                           aria-controls="collapseEightEservice" class="accordion-toggle">
                                            WITHDRAW SUBJECT a.k.a QUIT STUDIES (UKR11)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseEightEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingEightEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Withdraw/Quit
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Fill up Withdraw/Quit application
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>
                                                        Learning Centre
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingNineEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseNineEservice" aria-expanded="false"
                                           aria-controls="collapseNineEservice" class="accordion-toggle">
                                            CHANGE LEARNING CENTRE (UKR12)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseNineEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingNineEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Change Learning Centre
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Reason for Changing Learning Centre
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>
                                                        Learning Centre
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingTenEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseTenEservice" aria-expanded="false"
                                           aria-controls="collapseTenEservice" class="accordion-toggle">
                                            ONLINE LEARNING (OL) (UKR16)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTenEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingTenEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Online Learning (OL)
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Choose subject for online learning
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>
                                                        Record and Operations Department
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="headingElevenEservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapseElevenEservice" aria-expanded="false"
                                           aria-controls="collapseElevenEservice" class="accordion-toggle">
                                            DISCOUNT ON STUDY FEES (OKU / SENIORCITIZEN)(UKR18)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseElevenEservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="headingElevenEservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        OUM Portal
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        eService
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        UKR Online
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        On Study Fees (OKU / Senior Citizen)
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Identification Card No. for the<br/>physically
                                                        disabled & type of disability
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Preview & Submit Application Form to<br/>Record
                                                        and Operations Department
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading" role="tab" id="heading12Eservice">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordionEservice"
                                           href="#collapse12Eservice" aria-expanded="false"
                                           aria-controls="collapse12Eservice" class="accordion-toggle">
                                            ACTIVATION (UKR17)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse12Eservice" class="panel-collapse collapse"
                                     role="tabpanel"
                                     aria-labelledby="heading12Eservice">
                                    <div class="panel-body">
                                        <div class="col-lg-10 col-md-offset-4">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Visit Learning Centre
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        Fill Up form UKR17(Manual)
                                                    </a>
                                                    <div class="row">
                                                        <div class="col-xs-12 text-center">
                                                            <p class="btn">
                                                                <span
                                                                    class="fa fa-2x fa-angle-double-down arrow-light-blue"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <a href="#"
                                                       class="btn btn-success-1 btn-block center-block disabled"
                                                       role="button">
                                                        LC will submit Application Form<br/>to Records
                                                        and Operations Department
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="panel panel-primary">-->
                            <!--                                <div class="panel-heading" role="tab" id="heading13Eservice">-->
                            <!--                                    <a class="collapsed" data-toggle="collapse"-->
                            <!--                                       data-parent="#accordionEservice"-->
                            <!--                                       href="#collapse13Eservice" aria-expanded="false"-->
                            <!--                                       aria-controls="collapse13Eservice">-->
                            <!--                                        <h4 class="panel-title">COURSE EXEMPTION (UKR21)</h4>-->
                            <!--                                    </a>-->
                            <!--                                </div>-->
                            <!--                                <div id="collapse13Eservice" class="panel-collapse collapse"-->
                            <!--                                     role="tabpanel"-->
                            <!--                                     aria-labelledby="heading13Eservice">-->
                            <!--                                    <div class="panel-body">-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                        </div>

                    </div>
                </div>
            </section>
        </div>
        <div role="tabpanel" class="tab-pane" id="8">
            <br/>
            <h3 class="text-primary">eCRM
                <small class="text-info">(Complaint/Compliment/Enquiries/Suggestion)</small>
            </h3>
            <img src="../public/images/myvle/ecrm.png"
                 alt="eCRM(Complaint/Compliment/Enquiries/Suggestion)" class="img-thumbnail img-responsive"/>
        </div>
    </div>
</div>