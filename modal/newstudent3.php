<div class="row">
    <div class="col-md-12">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-group" id="accordion-payment" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-payment"
                               href="#paymentCollapseOne"
                               aria-expanded="true" aria-controls="collapseOne" class="accordion-toggle">
                                <i class="fa fa-bank"></i> Internet Banking
                            </a>
                        </h4>
                    </div>
                    <div id="paymentCollapseOne" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <a href="https://www.bankislam.biz/" target="_blank">
                                        <img src="../public/images/bank/bankislam.png"
                                             class="img-thumbnail img-responsive hvr-float-shadow"/>
                                    </a>
                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="http://cimbclicks.com.my/" target="_blank">
                                        <img src="../public/images/bank/cimbank.png"
                                             class="img-thumbnail img-responsive hvr-float-shadow"/>
                                    </a>
                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="http://www.maybank2u.com.my/" target="_blank">
                                        <img src="../public/images/bank/maybank.png"
                                             class="img-thumbnail img-responsive hvr-float-shadow"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a  class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion-payment"
                               href="#paymentCollapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa fa-credit-card"></i> Credit Card
                            </a>
                        </h4>
                    </div>
                    <div id="paymentCollapseTwo" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <img src="../public/images/bank/visamaster.png"
                                         class="img-thumbnail img-responsive"/>
                                </div>
                            </div>
                            <p class="text-center text-primary">OUM Main Campus & Learning Centre Only</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a  class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion-payment"
                               href="#paymentCollapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa fa-envelope-o"></i> Cheque/Money Order/Postal Order
                            </a>
                        </h4>
                    </div>
                    <div id="paymentCollapseThree" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingThree">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 text-center">
                                    <i class="fa fa-3x fa-envelope text-dark"></i>
                                </div>
                            </div>
                            <p class="text-center text-primary">Payee: UNIVERSITI TERBUKA MALAYSIA</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a  class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion-payment"
                               href="#paymentCollapseFour" aria-expanded="false" aria-controls="paymentCollapseThree">
                                <i class="fa fa-money"></i> Cash Payment
                            </a>
                        </h4>
                    </div>
                    <div id="paymentCollapseFour" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="headingFour">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="../public/images/bank/bankislam.png"
                                         class="img-thumbnail img-responsive"/>
                                    <ul class="text-primary">
                                        <li>Bank Islam Malaysia Berhad</li>
                                        <li>Payment made to UNITEM</li>
                                        <li>Account No: 14041-01-0053385</li>
                                        <li>Payment slip can be obtained from Bank Islam branches</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <img src="../public/images/bank/bsn.png" class="img-thumbnail img-responsive"/>
                                    <ul class="text-primary">
                                        <li>GIRO Bank Simpanan Nasional</li>
                                        <li>Payment made to UNIVERSITI TERBUKA MALAYSIA</li>
                                        <li>Account No: 14100-29-00022495-2</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>