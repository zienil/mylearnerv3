<div>
    <div class="row">
        <div class="col-xs-12">
            <p class="btn lead text-center bg-primary text-info center-block">Attend Final Exam</p>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="btn lead text-center bg-info text-info center-block">Did you sit for exam?</p>
            <div class="row">
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-center">
            <p class="center-block"><span class="btn btn-success btn-lg">Yes</span></p>
            <p class="btn center-block"><span class="glyphicon glyphicon-arrow-down"></span></p>

            <p class="btn bg-success text-success">
                Your grade will appear in your result examination slip / partial transcript
            </p>
        </div>
        <div class="col-xs-6 text-center">
            <p class="center-block"><span class="btn btn-danger btn-lg">No</span></p>
            <p class="btn center-block"><span class="glyphicon glyphicon-arrow-down"></span></p>
            <p class="btn bg-info text-info">Did you complete Borang UP17</p>
            <div class="row">
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span>
                </div>
                <div class="col-xs-6 text-center">
                    <p class="btn">
                        <span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <p class="center-block"><span class="btn-success btn-lg">Yes</span></p>
                    <p class="btn">
                        <span class="glyphicon glyphicon-arrow-down"></span></p>
                    <p class="btn bg-success text-success text-wrap" id="finalExamReason-btn" data-toggle="collapse"
                       data-target="#finalExamReason" aria-expanded="false" aria-controls="finalExamReason">
                        Incomplete <strong>"I"</strong> grade will appear in your result examination slip / partial
                        transcript
                    </p>

                </div>
                <div class="col-xs-6 text-center">
                    <p class="center-block"><span class="btn-danger btn-lg">No</span></p>
                    <p class="btn center-block"><span class="glyphicon glyphicon-arrow-down"></span></p>
                    <p class="btn bg-danger text-danger text-wrap" id="finalExamWOReason-btn"
                       data-toggle="collapse" data-target="#finalExamWOReason" aria-expanded="false"
                       aria-controls="finalExamWOReason">
                        Fail <strong>"F"</strong> grade will appear in your result examination slip / partial transcript
                    </p>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="collapse" id="finalExamReason">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h2 class="panel-title text-center">FINAL EXAM WITH REASON</h2>
            </div>
            <div class="panel-body panel-content">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <strong>"I" - Incomplete</strong> : Awarded when a student is unable to take the examination for
                        a course for any of the following reasons:
                        <ol type="a">
                            <li>Official company duties such as attendance at courses, seminars within or outside the
                                country and work commitments that cannot be postpone
                            </li>
                            <li>Medical treatment exceeding a day, maternity leave and or other medical condition
                                requiring extended period of treatment
                            </li>
                            <li>Emergency situations such as deceased of an immediate relative</li>
                        </ol>
                    </div>
                </div>
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills nav-justified" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#incompletePostgraduate" aria-controls="incompletePostgraduate" role="tab"
                               data-toggle="tab">
                                Incomplete Status (Postgraduate)
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#incompleteUndergraduate" aria-controls="incompleteUndergraduate" role="tab"
                               data-toggle="tab">
                                Incomplete Status (Undergraduate)
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="incompletePostgraduate">
                            <br/>
                            <div class="col-lg-10 col-md-offset-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">OUM Portal</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">eService</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Postgraduate Services & Support
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Exam Online</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Online Application Form
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Versi Bahasa Malaysia - Permohonan<br/>penangguhan peperiksaan
                                            akhir (UP17)
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">English Version – Postponement<br/>of final exam (UP17)
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Click OK
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="incompleteUndergraduate">
                            <br/>
                            <div class="col-lg-10 col-md-offset-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">OUM Portal</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">eService</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Undergraduate Services & Support
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Exam Online</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Online Application Form
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Versi Bahasa Malaysia - Permohonan<br/>penangguhan peperiksaan
                                            akhir (UP17)
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">English Version – Postponement<br/>of final exam (UP17)
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 text-center marging-arrow">
                                        <i class="fa fa-2x fa-angle-double-down arrow-light-blue"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#"
                                           class="btn btn-success-1 btn-block disabled"
                                           role="button">Click OK
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="collapse" id="finalExamWOReason">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h2 class="panel-title text-center">FINAL EXAM WITHOUT REASON</h2>
            </div>
            <div class="panel-body panel-content">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><strong>Absence or Withdrawal From Semester Examination</strong>
                        <ol type="a">
                            <li>If you intend to withdraw from a semester examination in one or more course(s) on
                                medical or other valid reasons, you should notify <strong>officially</strong> the
                                Assessment and Examination Unit, Registry <strong>not later than two weeks</strong>
                                before the semester examination concerned commences. A copy of your letter should also
                                be forwarded to the Dean of your Faculty. Supporting documents from doctor or employer
                                where applicable, should also be attached with your letter.
                            </li>
                            <li>Unless you have withdrawn officially from the examination for a registered course or
                                have valid reasons for your absence from the examination as approved by the University,
                                you shall be awarded grade <strong>"F"</strong> if you fail to take the examination for
                                the course concerned.
                            </li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>