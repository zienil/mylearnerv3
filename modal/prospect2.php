<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active">
            <a href="#undergraduate" aria-controls="undergraduate" role="tab" data-toggle="tab">
                Undergraduate Programs
            </a>
        </li>
        <li role="presentation">
            <a href="#postgraduate" aria-controls="postgraduate" role="tab" data-toggle="tab">
                Postgraduate Programs
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="undergraduate">
            <br/>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">BUSINESS & MANAGEMENT</h3>
                        </div>
                        <div class="panel-body">
                            <ol>
                                <li>Diploma in Management (DIM)</li>
                                <li>Diploma in Human Resource Management (DHRM)</li>
                                <li>Bachelor of Business Administration with Honours (BBA)</li>
                                <li>Bachelor of Management with Honours (BIM)</li>
                                <li>Bachelor of Accounting with Honours (BAC)</li>
                                <li>Bachelor of Human Resource Management with Honours (BHRM)</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ALLIED SCIENCES</h3>
                        </div>
                        <div class="panel-body">
                            <ol>
                                <li>Diploma in Information Technology (DIT)</li>
                                <li>Bachelor of Occupational Health and Safety Management with Honours (BOHSM)</li>
                                <li>Bachelor of Information Technology with Honours (BIT)</li>
                                <li>Bachelor of Manufacturing Management with Honours (BMMG)</li>
                                <li>Bachelor of Science in Project and Facility Management with Honours (BPFM)</li>
                                <li>Bachelor of Nursing Science with Honours (BNS)</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">EDUCATION AND SOCIAL SCIENCES</h3>
                        </div>
                        <div class="panel-body">
                            <ol>
                                <li>Diploma in Islamic Studies with Education (DIS)</li>
                                <li>Diploma in Early Childhood Education (DECE)</li>
                                <li>Bachelor of Islamic Studies (Islamic Management) with Honours (BIS)</li>
                                <li>Bachelor of Psychology with Honours (BPSY)</li>
                                <li>Bachelor of Communication with Honours (BCOM)</li>
                                <li>Bachelor of Education (TESL) with Honours (BETESL)</li>
                                <li>Bachelor of Early Childhood Education with Honours (BECE)</li>
                                <li>Bachelor or Teaching (Primary Education) with Honours (BTPE)</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="postgraduate">
            <br/>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">BUSINESS & MANAGEMENT</h3>
                        </div>
                        <div class="panel-body">
                            <ol>
                                <li>Master of Human Resource Management (MHRM)</li>
                                <li>Master of Business Administration (MBA)</li>
                                <li>Master of Management (MM)</li>
                                <li>Master of Business Administration - Fully Online (MBA)</li>
                                <li>Master of Management - Fully Online (MM)</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ALLIED SCIENCES</h3>
                        </div>
                        <div class="panel-body">
                            <ol>
                                <li>Master of Project Management (MPM)</li>
                                <li>Master of Occupational Safety and Health Risk Management (MOSHRM)</li>
                                <li>Master of Nursing (MN)</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">EDUCATION AND SOCIAL SCIENCES</h3>
                        </div>
                        <div class="panel-body">
                            <ol>
                                <li>Postgraduate Diploma in Teaching (PGDT)</li>
                                <li>Master of Islamic Studies (MIST)</li>
                                <li>Master of Corporate Communication (MCC)</li>
                                <li>Master of Counselling (MC)</li>
                                <li>Master in Early Childhood Education (MECHE)</li>
                                <li>Master of Instructional Design and Technology (MIDT)</li>
                                <li>Master of Education (MEd)</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>