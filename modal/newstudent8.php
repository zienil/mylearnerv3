<div>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">My Profile</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Examination</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Examination Timetable</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Print</a>
        </div>
    </div>
</div>