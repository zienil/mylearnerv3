<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active">
            <a href="#g1" aria-controls="g1" role="tab" data-toggle="tab">Graduation – Check Qualification</a>
        </li>
        <li role="presentation">
            <a href="#g2" aria-controls="g2" role="tab" data-toggle="tab">Graduation – Check Status</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="g1">
            <br/>
            <div class="row">
                <div class="col-lg-10 col-md-offset-4">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">myProfile</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Academic Progress</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Total credit to be
                                completed = 0
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="g2">
            <br/>
            <div class="row">
                <div class="col-lg-10 col-md-offset-4">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Convocation</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Semakan Graduan</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Insert IC</a>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Status</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>