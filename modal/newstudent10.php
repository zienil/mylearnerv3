<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active">
            <a href="#x1" aria-controls="x1" role="tab" data-toggle="tab">Grade</a>
        </li>
        <li role="presentation">
            <a href="#x2" aria-controls="x2" role="tab" data-toggle="tab">
                Grading scheme
            </a>
        </li>
        <li role="presentation">
            <a href="#x4" aria-controls="x4" role="tab" data-toggle="tab">
                Check Result
            </a>
        </li>
        <li role="presentation">
            <a href="#x5" aria-controls="x5" role="tab" data-toggle="tab">
                Print Result
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="x1">
            <br/>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Grade</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-md-4 text-center marging-arrow">
                    <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                </div>
                <div class="col-xs-4 col-md-4 text-center marging-arrow">
                    <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                </div>
                <div class="col-xs-4 col-md-4 text-center marging-arrow">
                    <i class="fa fa-2x fa-long-arrow-down" style="margin-right: 50%;"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-md-4 margin-list-group">
                    <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Pass</a>
                    <div class="row">
                        <div class="col-xs-8 col-md-8 text-center marging-arrow">
                            <i class="fa fa-2x fa-long-arrow-down"></i>
                        </div>
                    </div>
                    <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Preparation for next<br/>semester</a>
                </div>
                <div class="col-xs-4 col-md-4 margin-list-group">
                    <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Fail</a>
                    <div class="row">
                        <div class="col-xs-4 col-md-5 text-center marging-arrow">
                            <i class="fa fa-2x fa-long-arrow-down"></i>
                        </div>
                        <div class="col-xs-4 col-md-2 text-center"></div>
                        <div class="col-xs-4 col-md-5 text-center marging-arrow">
                            <i class="fa fa-2x fa-long-arrow-down"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-md-6 text-center">
                            <div class="list-group">
                                <a href="#" id="refer-btn" class="btn btn-primary btn-block link hvr-buzz-out" role="button"
                                   data-toggle="collapse"
                                   data-target="#refer" aria-expanded="false"
                                   aria-controls="refer" style="text-align: center;">Refer</a>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <div class="list-group">
                                <a href="#" id="repeat-btn" class="btn btn-primary btn-block link hvr-buzz-out" role="button"
                                   data-toggle="collapse"
                                   data-target="#repeat" aria-expanded="false"
                                   aria-controls="repeat" style="text-align: center;">Repeat</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-md-4 margin-list-group">
                    <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Incomplete</a>
                    <div class="row">
                        <div class="col-xs-8 col-md-8 text-center marging-arrow">
                            <i class="fa fa-2x fa-long-arrow-down"></i>
                        </div>
                    </div>
                    <a href="#" class="btn btn-success-1 btn-block disabled" role="button">
                        Register for final<br/>exam(within 1 year)</a>
                </div>
            </div>
            <br/>
            <div class="collapse" id="refer">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">REFER</h2>
                    </div>
                    <div class="panel-body panel-content">
                        <ol>
                            <li>To repeat part of assessment marks (final examination only).</li>
                            <li>Reasons :</li>
                            <ul>
                                <li>To complete "<strong>I</strong>" status</li>
                                <li>To improve final examination grade</li>
                            </ul>
                            <li>You must register to retake the examination component of the course when the course is
                                offered again in a subsequent semester (duration 1 year).
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="collapse" id="repeat">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h2 class="panel-title text-center">REPEAT</h2>
                    </div>
                    <div class="panel-body panel-content">
                        <ol>
                            <li>To repeat a whole course (assignment and final examination).</li>
                            <li>Reasons :</li>
                            <ul>
                                <li>To improve original grade for the course when:</li>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-condensed">
                                        <thead>
                                        <tr class="warning">
                                            <th>Assessment</th>
                                            <th>Original grade</th>
                                        </tr>
                                        <tbody>
                                        <tr>
                                            <td>Assignment</td>
                                            <td>Result less than 40% of overall marks</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="warning"><strong>Final Exam</strong></td>
                                        </tr>
                                        <tr>
                                            <td>MPW course</td>
                                            <td>Result below than C</td>
                                        </tr>
                                        <tr>
                                            <td>Undergraduate course</td>
                                            <td>Result is F</td>
                                        </tr>
                                        <tr>
                                            <td>Postgraduate course</td>
                                            <td>Result below than B</td>
                                        </tr>
                                        </tbody>
                                        </thead>
                                    </table>
                                </div>
                            </ul>
                            <li>You must re-register for the course that you had registered previously, undergo again
                                the
                                continuous assessment components and retake the examination for the course.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="x2">
            <br/>
            <ul class="nav nav-pills nav-justified" role="tablist">
                <li role="presentation" class="active">
                    <a href="#undergraduate" aria-controls="undergraduate" role="tab" data-toggle="tab">Undergraduate</a>
                </li>
                <li role="presentation">
                    <a href="#postgraduate" aria-controls="postgraduate" role="tab" data-toggle="tab">Postgraduate</a>
                </li>
            </ul>
            <br/>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="undergraduate">
                    <div class="panel panel-warning">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr class="warning">
                                        <th class="text-center">Grade</th>
                                        <th class="text-center">Grade Point</th>
                                        <th class="text-center">Mark Equivalent</th>
                                        <th class="text-center">Descriptor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="text-center success">
                                        <td>A</td>
                                        <td>4.00</td>
                                        <td>80-100</td>
                                        <td>Outstanding</td>
                                    </tr>
                                    <tr class="text-center success">
                                        <td>A -</td>
                                        <td>3.67</td>
                                        <td>75-79</td>
                                        <td>Very Good</td>
                                    </tr>
                                    <tr class="text-center info">
                                        <td>B +</td>
                                        <td>3.33</td>
                                        <td>70-74</td>
                                        <td rowspan="3" style="vertical-align: middle;">Good</td>
                                    </tr>
                                    <tr class="text-center info">
                                        <td>B</td>
                                        <td>3.00</td>
                                        <td>65-69</td>
                                    </tr>
                                    <tr class="text-center info">
                                        <td>B -</td>
                                        <td>2.67</td>
                                        <td>60-64</td>
                                    </tr>
                                    <tr class="text-center info">
                                        <td>C +</td>
                                        <td>2.33</td>
                                        <td>55-59</td>
                                        <td rowspan="2" style="vertical-align: middle;">Pass</td>
                                    </tr>
                                    <tr class="text-center info">
                                        <td>C</td>
                                        <td>2.00</td>
                                        <td>50-54</td>
                                    </tr>
                                    <tr class="text-center warning">
                                        <td>C -</td>
                                        <td>1.67</td>
                                        <td>45-49</td>
                                        <td rowspan="3" style="vertical-align: middle;"><strong>Conditional Pass<br/>(Subject to CGPA => 2.00)</strong></td>
                                    </tr>
                                    <tr class="text-center warning">
                                        <td>D +</td>
                                        <td>1.33</td>
                                        <td>40-44</td>
                                    </tr>
                                    <tr class="text-center warning">
                                        <td>D</td>
                                        <td>1.00</td>
                                        <td>35-39</td>
                                    </tr>
                                    <tr class="text-center danger">
                                        <td>F</td>
                                        <td>0.00</td>
                                        <td>0-34</td>
                                        <td>Fail</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="postgraduate">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                            <tr class="warning">
                                <th rowspan="2" class="text-center" style="vertical-align: middle;">Grade</th>
                                <th rowspan="2" class="text-center" style="vertical-align: middle;">Grade Point</th>
                                <th rowspan="2" class="text-center" style="vertical-align: middle;">Mark Equivalent</th>
                                <th colspan="2" class="text-center">Meaning</th>
                            </tr>
                            <tr class="warning">
                                <th class="text-center">Before January 2010</th>
                                <th class="text-center">From January 2010 Onwards</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="text-center success">
                                <td>A</td>
                                <td>4.00</td>
                                <td>80-100</td>
                                <td>Outstanding</td>
                                <td>Outstanding</td>
                            </tr>
                            <tr class="text-center success">
                                <td>A -</td>
                                <td>3.67</td>
                                <td>75-79</td>
                                <td>Very Good</td>
                                <td>Very Good</td>
                            </tr>
                            <tr class="text-center info">
                                <td>B +</td>
                                <td>3.33</td>
                                <td>70-74</td>
                                <td>Good</td>
                                <td>Good</td>
                            </tr>
                            <tr class="text-center info">
                                <td>B</td>
                                <td>3.00</td>
                                <td>65-69</td>
                                <td>Pass</td>
                                <td>Pass</td>
                            </tr>
                            <tr class="text-center warning">
                                <td>B -</td>
                                <td>2.67</td>
                                <td>60-64</td>
                                <td rowspan="6" style="vertical-align: middle;"><strong>Marginal Failure<br/>(Subject To CGPA => 3.00)</strong></td>
                                <td rowspan="3" style="vertical-align: middle;"><strong>Conditional Pass<br/>(Subject To CGPA => 3.00)</strong></td>
                            </tr>
                            <tr class="text-center warning">
                                <td>C +</td>
                                <td>2.33</td>
                                <td>55-59</td>
                            </tr>
                            <tr class="text-center warning">
                                <td>C</td>
                                <td>2.00</td>
                                <td>50-54</td>
                            </tr>
                            <tr class="text-center warning">
                                <td>C -</td>
                                <td>1.67</td>
                                <td>45-49</td>
                                <td rowspan="4" style="vertical-align: middle;" class="danger">Fail</i></td>
                            </tr>
                            <tr class="text-center warning">
                                <td>D +</td>
                                <td>1.33</td>
                                <td>40-44</td>
                            </tr>
                            <tr class="text-center warning">
                                <td>D</td>
                                <td>1.00</td>
                                <td>35-39</td>
                            </tr>
                            <tr class="text-center danger">
                                <td>F</td>
                                <td>0.00</td>
                                <td>0-34</td>
                                <td>Fail</td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr class="text-center">
                                <td>E</td>
                                <td>-</td>
                                <td>-</td>
                                <td colspan="2" class="text-center">Excellent</td>
                            </tr>
                            <tr class="text-center">
                                <td>G</td>
                                <td>-</td>
                                <td>-</td>
                                <td colspan="2" class="text-center">Good</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="x4">
            <br/>
            <div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">My Profile</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Examination</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Current Result</a>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="x5">
            <br/>
            <div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">My Profile</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Examination</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Print Exam Result
                            Slip</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Select Semester</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Select Language</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Print Exam Slip</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>