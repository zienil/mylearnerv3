<div>
    <div class="row">
        <div class="col-lg-10 col-md-offset-4">
            <div class="row">
                <div class="col-md-5">
                    <a href="http://fastrac.oum.edu.my" target="_blank"
                       class="btn btn-primary btn-block hvr-float-shadow" role="button">
                        Login : http://fastrac.oum.edu.my
                    </a>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <p class="btn"><span class="fa fa-2x fa-angle-double-down arrow-light-blue"></span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <a href="#" class="btn btn-info-1 btn-block disabled" role="button">
                        Download Letter of Admission
                    </a>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="panel-group" id="accordion-acknowledgement" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion-acknowledgement"
                       href="#acknowledgementCollapseOne"
                       aria-expanded="true" aria-controls="collapseOne" class="accordion-toggle">
                        Sample of Acknowledgement of Acceptance
                    </a>
                </h4>
            </div>
            <div id="acknowledgementCollapseOne" class="panel-collapse collapse in" role="tabpanel"
                 aria-labelledby="headingOne">
                <div class="panel-body">
                    <a href="http://fastrac.oum.edu.my" target="_blank">
                        <img src="../public/images/all/letterOfAdminsion.png" class="img-thumbnail img-responsive">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>