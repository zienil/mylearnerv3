<div>
    <img src="../public/images/all/importantDocs.png" class="img-thumbnail img-responsive">
</div>
<hr/>
<div class="panel-group" id="accordion-kit" role="tablist" aria-multiselectable="true">
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion-kit" href="#kitCollapseOne"
                   aria-expanded="true" aria-controls="collapseOne" class="accordion-toggle">
                    Sample : Letter of Admission
                </a>
            </h4>
        </div>
        <div id="kitCollapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <img src="../public/images/all/letterOfAdminsion.png" class="img-thumbnail img-responsive">
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion-kit"
                   href="#kitCollapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="accordion-toggle">
                    Sample : Registration Guide
                </a>
            </h4>
        </div>
        <div id="kitCollapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <img src="../public/images/all/registration_guide.png" class="img-thumbnail img-responsive">
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion-kit"
                   href="#kitCollapseThree" aria-expanded="false" aria-controls="kitCollapseThree"
                   class="accordion-toggle">
                    Sample : Registration Form
                </a>
            </h4>
        </div>
        <div id="kitCollapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <img src="../public/images/all/registration_form.png" class="img-thumbnail img-responsive">
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion-kit"
                   href="#kitCollapseFour" aria-expanded="false" aria-controls="collapseThree" class="accordion-toggle">
                    Sample : Declaration Sheet
                </a>
            </h4>
        </div>
        <div id="kitCollapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
                <img src="../public/images/all/declarationSheet.png" class="img-thumbnail img-responsive">
            </div>
        </div>
    </div>
</div>