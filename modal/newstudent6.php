<div>
    <div class="row">
        <div class="col-xs-12">
            <p class="lead text-center bg-success btn text-success center-block">Submission Modes</p>
            <div class="row">
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p></div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-center">
            <p id="on9Submit-btn" class="btn btn-block bg-primary text-danger hvr-buzz-out" data-toggle="collapse" data-target="#on9Submit"
               aria-expanded="false" aria-controls="on9Submit">Online Submission</p>
        </div>
        <div class="col-xs-6 text-center">
            <p class="btn btn-block bg-success text-success">Face 2 Face Submission</p>
            <div class="row">
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
                <div class="col-xs-6 text-center">
                    <p class="btn"><span class="glyphicon glyphicon-arrow-down"></span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <p class="btn btn-block bg-primary text-danger hvr-buzz-out text-wrap" id="f2fSubmit-btn"
                       data-toggle="collapse" data-target="#f2fSubmit" aria-expanded="false"
                       aria-controls="f2fSubmit">Face 2 Face Learners</p>
                </div>
                <div class="col-xs-6 text-center">
                    <p class="btn btn-block bg-primary text-danger hvr-buzz-out text-wrap" id="f2fon9Submit-btn"
                       data-toggle="collapse" data-target="#f2fon9Submit" aria-expanded="false"
                       aria-controls="f2fon9Submit">Online Learners</p>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="collapse" id="on9Submit">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h2 class="panel-title text-center">Online Submission</h2>
            </div>
            <div class="panel-body panel-content">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">myInspire - Choose
                            Subject</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Assignment</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Assignment Question &
                            Submission : Choose Submission</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Choose file to upload</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Submit assignment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse" id="f2fSubmit">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h2 class="panel-title text-center">Face 2 Face Submission</h2>
            </div>
            <div class="panel-body panel-content">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Format Checking</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Print Final Copy</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Submit to Face 2 Face
                            Tutor</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Assignment Submission
                            Receipt</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="collapse" id="f2fon9Submit">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h2 class="panel-title text-center">Face 2 Face Submission (Online Learners)</h2>
            </div>
            <div class="panel-body panel-content">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Format Checking</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Print Final Copy</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Refer faculty
                            announcement for submission mode</a>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <p class="btn"><span class="fa fa-2x fa-angle-double-down"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2">
                        <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Assignment
                            Submission Receipt</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>