<div>
    <!-- Nav tabs -->
    <ul class="nav nav-pills nav-justified" role="tablist">
        <li role="presentation" class="active">
            <a href="#psh" aria-controls="psh" role="tab" data-toggle="tab">Postgraduate Student Handbook</a>
        </li>
        <li role="presentation">
            <a href="#ush" aria-controls="ush" role="tab" data-toggle="tab">Undergraduate Student Handbook</a>
        </li>
    </ul>
    <br/>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="psh">
            <p>If PDF is not loading, please click
                <a href="/public/pdf/Postgraduate_Student_Handbook.pdf" target="_blank">here</a>
            </p>
            <div class="embed-responsive embed-responsive-16by9">
                <object class="embed-responsive-item" type="application/pdf"
                        data="/public/pdf/Postgraduate_Student_Handbook.pdf"></object>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="ush">
            <p>If PDF is not loading, please click
                <a href="/public/pdf/Undergraduate_Student_Handbook.pdf" target="_blank">here</a>
            </p>
            <div class="embed-responsive embed-responsive-16by9">
                <object class="embed-responsive-item" type="application/pdf"
                        data="/public/pdf/Undergraduate_Student_Handbook.pdf"></object>
            </div>
        </div>
    </div>

</div>