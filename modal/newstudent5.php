<div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">OUM Portal</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn">
                        <span class="fa fa-2x fa-angle-double-down"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">myInspire : Choose Subject</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn">
                        <span class="fa fa-2x fa-angle-double-down"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Assignment</a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn">
                        <span class="fa fa-2x fa-angle-double-down"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Assignment Question & Submission :
                Choose Semester </a>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="btn">
                        <span class="fa fa-2x fa-angle-double-down"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <a href="#" class="btn btn-success-1 btn-block disabled" role="button">Download</a>
        </div>
    </div>
</div>