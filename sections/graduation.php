<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper"><!--.page-title-content-->
            <!--            <h2 class="captions">graduate student</h2>-->
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">Graduating Student</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding prospect-section choose-course-2 background-opacity padding-bottoms">
    <div class="container">
        <div class="group-title-index">
            <h2 class="center-title">YOUR JOURNEY AS A GRADUATING STUDENT</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-graduation-cap" aria-hidden="true"></i></div>
        </div>
        <div class="choose-course-wrapper">
            <div class="row">
                <div class="col-xs-3 col-md-3">
                    <div class="graduatingstudent-right wow fadeInLeftBig" data-wow-duration="2s">
                        <h4>
                            <img src="../public/images/customeIcons/Registration.png"
                                 class="tutorial_popover center-block img-responsive pointer"
                                 data-toggle="popover" aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#registration1" title="Registration">
                            registration
                        </h4>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3">
                    <div class="graduatingstudent-right wow fadeInLeftBig" data-wow-duration="3s">
                        <h4>
                            <img src="../public/images/customeIcons/Practicum.png" class="center-block img-responsive">
                            intra/project paper
                        </h4>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3">
                    <div class="graduatingstudent-right wow fadeInLeftBig" data-wow-duration="4s">
                        <h4>
                            <img src="../public/images/customeIcons/GraduatingStudent.png"
                                 class="tutorial_popover center-block img-responsive pointer"
                                 data-toggle="popover" aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#graduation" title="Graduation">
                            graduation
                        </h4>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3">
                    <div class="graduatingstudent-end wow fadeInLeftBig" data-wow-duration="5s">
                        <h4>
                            <img src="../public/images/customeIcons/FurtherStudy.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#progOff"
                                 title="Programmes Offered">
                            further study
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
</div>