<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper"><!--.page-title-content-->
<!--            <h2 class="captions">OUM Learning Centre</h2>-->
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">Learning Centres</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding gallery-page gallery-4column lc-section background-opacity">
    <div class="container">
        <div class="group-title-index">
            <h4 class="top-title">Find One Nearest to You</h4>
            <h2 class="center-title">OUM Learning Centres</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-map-o" aria-hidden="true"></i></div>
        </div>
        <div class="gallery-nav">
            <ul id="filters" class="nav nav-tabs edugate-tabs button-group filters-button-group">
                <li data-filter="*" data-category="all" class="tab active">
                    <div class="text">all</div>
                </li>
                <li data-filter=".north" data-category="north" class="tab">
                    <div class="text">North</div>
                </li>
                <li data-filter=".klang" data-category="klang" class="tab">
                    <div class="text">Klang Valley</div>
                </li>
                <li data-filter=".south" data-category="south" class="tab">
                    <div class="text">South</div>
                </li>
                <li data-filter=".eastcost" data-category="eastcost" class="tab">
                    <div class="text">East Cost</div>
                </li>
                <li data-filter=".sabah" data-category="sabah" class="tab">
                    <div class="text">Sabah</div>
                </li>
                <li data-filter=".sarawak" data-category="sarawak" class="tab">
                    <div class="text">Sarawak</div>
                </li>
            </ul>
        </div>
        <div class="grid">
            <div data-category="north" class="grid-item north" data-toggle="modal" data-target="#lc-kedahperlis">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Kedah & Perlis</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/kedah.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="eastcost" class="grid-item eastcost" data-toggle="modal" data-target="#lc-pahang">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Pahang</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/pahang.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="klang" class="grid-item klang" data-toggle="modal" data-target="#lc-kl">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Kuala Lumpur</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/kualalumpur.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="north" class="grid-item north" data-toggle="modal" data-target="#lc-penang">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Penang</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/penang.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="klang" class="grid-item klang" data-toggle="modal" data-target="#lc-selangor">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Selangor</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/selangor.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="south" class="grid-item south" data-toggle="modal" data-target="#lc-n9">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Negeri Sembilan</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/negeri.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="eastcost" class="grid-item eastcost" data-toggle="modal" data-target="#lc-kelantan">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Kelantan</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/kelantan.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="south" class="grid-item south" data-toggle="modal" data-target="#lc-melaka">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Melaka</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/melaka.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="eastcost" class="grid-item eastcost" data-toggle="modal" data-target="#lc-terengganu">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Terengganu</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/terengganu.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="sabah" class="grid-item sabah" data-toggle="modal" data-target="#lc-sabah">
                <a href="#" class="link"><span
                        class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Sabah</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/sabah.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="north" class="grid-item north" data-toggle="modal" data-target="#lc-perak">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Perak</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/perak.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="sarawak" class="grid-item sarawak" data-toggle="modal" data-target="#lc-sarawak">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i><span>Sarawak</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/sarawak.png" class="img-responsive img-circle"/>
                </a>
            </div>
            <div data-category="south" class="grid-item south" data-toggle="modal" data-target="#lc-johor">
                <a href="#" class="link">
                    <span class="content">
                        <span class="content-title">Learning Centre</span>
                        <span class="content-edu">
                            <i class="icons fa fa-map-marker" aria-hidden="true"></i>
                            <span>Johor</span>
                        </span>
                    </span>
                    <img src="../public/images/flag/johor.png" class="img-responsive img-circle"/>
                </a>
            </div>
        </div>
    </div>
</div>