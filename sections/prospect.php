<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper"><!--.page-title-content-->
            <!--            <h2 class="captions">prospect student</h2>-->
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">Prospective Student</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding prospect-section choose-course-2 background-opacity padding-bottoms">
    <div class="container">
        <div class="group-title-index">
            <h2 class="center-title">YOUR JOURNEY AS A PROSPECTIVE STUDENT</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-users" aria-hidden="true"></i></div>
        </div>
        <div class="choose-course-wrapper">
            <div class="row">
                <div class="col-xs-4 col-md-4">
                    <div class="prospect-right wow bounceInDown" data-wow-duration="3s">
                        <h4 class="hoverSound">
                            <img src="/public/images/customeIcons/WelcomeOUM.png"
                                 class="tutorial_popover center-block img-responsive pointer"
                                 data-toggle="popover" aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#welcome" title="Welcome To OUM">
                            Welcome to OUM
                        </h4>
                    </div>
                </div>
                <div class="col-xs-4 col-md-4">
                    <div class="prospect-right wow bounceInDown" data-wow-duration="4s">
                        <h4>
                            <img src="../public/images/customeIcons/ProgrammesOffered.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#progOff"
                                 title="Programmes Offered">
                            programmes offered
                        </h4>
                    </div>
                </div>
                <div class="col-xs-4 col-md-4">
                    <div class="prospect-right-end wow bounceInDown" data-wow-duration="5s">
                        <h4>
                            <img src="../public/images/customeIcons/OnlineApplication.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#on9Apps"
                                 title="Online Application">
                            Online Application
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
</div>