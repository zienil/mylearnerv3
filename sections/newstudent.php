<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper"><!--.page-title-content-->
            <!--            <h2 class="captions">new student</h2>-->
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">New Student</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding prospect-section choose-course-2 background-opacity">
    <div class="container">
        <div class="group-title-index">
            <h2 class="center-title">YOUR JOURNEY AS A NEW STUDENT</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-users" aria-hidden="true"></i></div>
        </div>
        <div class="choose-course-wrapper">
            <div class="row">
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right wow bounceInLeft" data-wow-duration="2s">
                        <h5>
                            <img src="../public/images/customeIcons/Registration.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#registration"
                                 title="Semester Start"> semester start
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right wow bounceInLeft" data-wow-duration="3s">
                        <h5>
                            <img src="../public/images/customeIcons/OUMLMS.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#myvle"
                                 title="OUM Portal – First Time Login"> OUM LMS
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right responsive-down-newstudent wow bounceInLeft" data-wow-duration="4s">
                        <h5>
                            <img src="../public/images/customeIcons/BKB.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#eservice"
                                 title="Gateway to OUM Virtual Learning Environment"> OUM Portal
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 right-responsive-newstudent">
                    <div class="newstudent-right responsive-left-newstudent wow bounceInLeft" data-wow-duration="5s">
                        <h5>
                            <img src="../public/images/customeIcons/TutorialPreparation.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#tutorial_preparation"
                                 title="Tutorial Preparation"> Tutorial prep
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 right-responsive-newstudent">
                    <div class="newstudent-right responsive-left-newstudent wow bounceInLeft" data-wow-duration="6s">
                        <h5>
                            <img src="../public/images/customeIcons/Eforum.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#eforum"
                                 title="eForum"> eforum
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-down wow bounceInLeft" data-wow-duration="7s">
                        <h5>
                            <img src="../public/images/customeIcons/Tutorial.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#tutorial_I"
                                 title="Tutorial I"> tutorial i
                        </h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-md-2 pull-right left-responsive-newstudent">
                    <div class="newstudent-left responsive-right-newstudent wow bounceInRight" data-wow-duration="2s">
                        <h5>
                            <img src="../public/images/customeIcons/Tutorial.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#tutorial_II"
                                 title="Tutorial II"> tutorial ii
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 pull-right left-responsive-newstudent">
                    <div class="newstudent-left responsive-right-newstudent wow bounceInRight" data-wow-duration="3s">
                        <h5>
                            <img src="../public/images/customeIcons/Pre-registration.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#pre_registration"
                                 title="Next Sem Pre-Registration"> NEXT SEM PRE-REG
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 pull-right left-responsive-newstudent">
                    <div class="newstudent-left responsive-down-newstudent wow bounceInRight" data-wow-duration="4s">
                        <h5>
                            <img src="../public/images/customeIcons/AssignmentPreparation.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#assignment_preparation"
                                 title="Assignment Preparation"> Assignment prep
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 pull-right left-responsive-newstudent">
                    <div class="newstudent-left responsive-down-newstudent wow bounceInRight" data-wow-duration="5s">
                        <h5>
                            <img src="../public/images/customeIcons/Tutorial.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#tutorial_III" title="Tutorial III">
                            tutorial iii
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 pull-right">
                    <div class="newstudent-left wow bounceInRight" data-wow-duration="6s">
                        <h5>
                            <img src="../public/images/customeIcons/AssignmentSubmission.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#assg_submit"
                                 title="Assignment Submission"> assign-submission
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 pull-right">
                    <div class="newstudent-down responsive-left-newstudent wow bounceInRight" data-wow-duration="7s">
                        <h5>
                            <img src="../public/images/customeIcons/Tutorial.png"
                                 class="tutorial_popover center-block img-responsive pointer"
                                 data-toggle="popover" aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#tutorial_IV" title="Tutorial IV"> tutorial iv
                        </h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right wow bounceInLeft" data-wow-duration="2s">
                        <h5>
                            <img src="../public/images/customeIcons/Result.png"
                                 class="tutorial_popover center-block img-responsive pointer"
                                 data-toggle="popover" aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#assign_result" title="Assignment Result"> assign result
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right wow bounceInLeft" data-wow-duration="2s">
                        <h5>
                            <img src="../public/images/customeIcons/ExamPreparation.png"
                                 class="tutorial_popover center-block img-responsive pointer"
                                 data-toggle="popover" aria-haspopup="true" aria-expanded="false"
                                 data-popover-content="#exam_preparation" title="Exam Preparation"> exam preparation
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right wow bounceInLeft" data-wow-duration="3s">
                        <h5>
                            <img src="../public/images/customeIcons/FinalExam.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-popover-content="#final_exam"
                                 title="Final Exam"> Final Exam
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="newstudent-right responsive-down-newstudent wow bounceInLeft" data-wow-duration="4s">
                        <h5>
                            <img src="../public/images/customeIcons/SemesterFinished.png"
                                 class="center-block img-responsive"> semester finished
                        </h5>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2 right-responsive-newstudent">
                    <div class="newstudent-end wow bounceInLeft" data-wow-duration="5s">
                        <h5>
                            <img src="../public/images/customeIcons/Result.png"
                                 class="tutorial_popover center-block img-responsive pointer" data-toggle="popover"
                                 aria-haspopup="true" aria-expanded="false" data-placement="left"
                                 data-popover-content="#result" title="Exam Result"> exam result
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>