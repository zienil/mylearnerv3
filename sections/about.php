<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper">
            <!--            <h2 class="captions">FAQ</h2>-->
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">Contact Us</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding about-section background-opacity">
    <div class="container">
        <div class="group-title-index">
            <h4 class="top-title">Find Out More About OUM</h4>
            <h2 class="center-title">CONTACT US</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-commenting-o"></i></div>
        </div>
    </div>
    <!--INTRO EDUGATE-->
    <div class="section intro-about">
        <div class="container">
            <div class="intro-about-wrapper">
                <div class="row">
                    <div class="col-md-5">
                        <img src="../public/images/OUMHQ2.jpg" alt="ITLA"
                             class="intro-image img-thumbnail img-responsive fadeInLeft animated wow"/>
                    </div>
                    <div class="col-md-7">
                        <div class="intro-title text-uppercase">University Profile</div>
                        <div class="intro-content">
                            <strong>Open University Malaysia</strong> is the seventh Malaysian private university
                            and it is owned by the <strong>Multimedia Technology Enhancement Operations (METEOR)
                                Sdn. Bhd </strong>, a consortium of 11 Malaysian public universities. It leverages
                            on the quality, prestige and capabilities of its consortium.
                        </div>
                        <div class="intro-title text-capitalize">Vision</div>
                        <div class="intro-content">
                            To be the Leading Provider of Flexible Learning
                        </div>
                        <div class="intro-title">Mission</div>
                        <div class="intro-content">
                            To widen access to quality education and provide lifelong learning opportunities by
                            leveraging on technology, adopting flexible mode of learning, and providing a conducive
                            and engaging learning environment at competitive and affordable cost
                            Shared Values:
                            <ul>
                                <li><strong>Integrity</strong></li>
                                <li><strong>Professionalism</strong></li>
                                <li><strong>Caring</strong></li>
                                <li><strong>Teamwork</strong></li>
                                <li><strong>Innovativeness</strong></li>
                            </ul>
                        </div>
                        <div class="intro-title">Centre of Learner Affairs (CLA)</div>
                        <div class="intro-content" style="padding-bottom: 30px;">
                            The Centre administers all matters pertaining to the provision of services to enrich
                            learner experience and knowledge mainly through:
                            <ul>
                                <li><strong>Student Retention Programmes</strong></li>
                                <li><strong>Student Development Programmes and Student Welfare</strong></li>
                                <li><strong>Student Support Services</strong></li>
                                <li><strong>Monitoring and Research</strong></li>
                                <li><strong>Alumni Relations</strong></li>
                            </ul>
                            All these functions are directed towards retaining students. CLA collaborates with other
                            faculties,centres,divisions and units in organising activities to helps students have a
                            dynamic and comprehensive learning experience. CLA is also responsible for disseminating
                            guidelines on and enforcing the Student Code of Ethics. It takes part in student
                            discipline committee meetings.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-padding edu-feature">
        <div class="container">
            <div class="group-title-index">
                <!--                <h4 class="top-title">If you have any questions, or are in any doubt.</h4>-->
                <h2 class="center-title">HOW TO CONTACT US ?</h2>
                <div class="bottom-title"><i class="bottom-icon fa fa-phone-square" aria-hidden="true"></i></div>
            </div>
            <div class="edu-feature-wrapper">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th class="danger text-warning">LSC Hotline</th>
                                            <td class="info text-primary" colspan="2">1-300-887-300</td>
                                        </tr>
                                        <tr>
                                            <th class="danger text-warning">myINSPIRE</th>
                                            <td class="info text-primary" colspan="2"><a
                                                    href="mailto:mylms_admin@oum.edu.my">mylms_admin@oum.edu.my</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="danger text-warning">Facsimile</th>
                                            <td class="info text-primary" colspan="2">03-2697 8853</td>
                                        </tr>
                                        <tr>
                                            <th class="danger text-warning" style="vertical-align: middle;">Walk-in
                                                Service
                                            </th>
                                            <td class="info text-primary">
                                                <address>
                                                    <strong>Open University Malaysia</strong><br>
                                                    Ground Floor, Zone C<br>
                                                    Jalan Tun Ismail<br>
                                                    50480 Kuala Lumpur.
                                                </address>
                                            </td>
                                            <td class="info text-primary">
                                                Operating Hours:
                                                <ul>
                                                    <li>Monday – Thursday: 8.00am – 5.30pm</li>
                                                    <li>Friday: 8.00am – 5.00pm</li>
                                                    <li>Saturday, Sunday & Public Holiday: CLOSED</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEST STAFF-->
    <div class="section section-padding staff-list best-staff" style="margin-bottom: -5%;">
        <div class="container">
            <div class="group-title-index">
                <h2 class="center-title">Production Team</h2>
                <div class="bottom-title"><i class="bottom-icon fa fa-at"></i></div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2 col-sm-4">
                    <div class="panel panel-default staff-item-wrapper wow bounceInDown" data-wow-duration="3s">
                        <div class="panel-body">
                            <img src="../public/images/aboutus/DrMansor.jpg"
                                 class="img-responsive img-circle center-block"/>
                            <br/>
                            <div class="staff-job text-center">ADVISOR</div>
                            <div class="staff-name text-center">Ybhg. Prof. Dato' Dr. Mansor Bin Fadzil</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="panel panel-default staff-item-wrapper wow bounceInDown" data-wow-duration="4s">
                        <div class="panel-body">
                            <img src="../public/images/aboutus/DrLim.jpg"
                                 class="img-responsive img-circle center-block"/>
                            <br/>
                            <div class="staff-job text-center">DIRECTOR</div>
                            <div class="staff-name text-center">Prof. Dr. Lim Tick Meng</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="panel panel-default staff-item-wrapper wow bounceInDown" data-wow-duration="5s">
                        <div class="panel-body">
                            <img src="../public/images/aboutus/Noral.jpg"
                                 class="img-responsive img-circle center-block"/>
                            <br/>
                            <div class="staff-job text-center">PROJECT COORDINATOR</div>
                            <div class="staff-name text-center">Noral Hidayah Binti Alwi</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="panel panel-default staff-item-wrapper wow bounceInDown" data-wow-duration="6s">
                        <div class="panel-body">
                            <img src="../public/images/aboutus/Shahrizan.jpg"
                                 class="img-responsive img-circle center-block"/>
                            <br/>
                            <div class="staff-job text-center">TECHNICAL SUPPORT I</div>
                            <div class="staff-name text-center">Mohd Shahrizan Bin Sukirman</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="panel panel-default staff-item-wrapper wow bounceInDown" data-wow-duration="7s">
                        <div class="panel-body">
                            <img src="../public/images/aboutus/Izral.jpg"
                                 class="img-responsive img-circle center-block"/>
                            <br/>
                            <div class="staff-job text-center">TECHNICAL SUPPORT II</div>
                            <div class="staff-name text-center">Mohd Izral Bin Mohd Nazir</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="panel panel-default staff-item-wrapper wow bounceInDown" data-wow-duration="8s">
                        <div class="panel-body">
                            <img src="../public/images/aboutus/oum.jpg" class="img-responsive img-circle center-block"/>
                            <br/>
                            <div class="staff-job text-center">COMMITTEE</div>
                            <div class="staff-name text-center">All ITLA Staff</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="section section-padding staff-list best-staff" style="margin-bottom: -5%;">-->
    <!--        <div class="container">-->
    <!--            <div class="group-title-index">-->
    <!--                <h2 class="center-title">Production Team</h2>-->
    <!--                <div class="bottom-title"><i class="bottom-icon fa fa-at"></i></div>-->
    <!--            </div>-->
    <!--            <div class="best-staff-wrapper">-->
    <!--                <div class="best-staff-content">-->
    <!--                    <div class="staff-item customize">-->
    <!--                        <div class="staff-item-wrapper">-->
    <!--                            <div class="staff-info">-->
    <!--                                <a href="#" class="staff-avatar">-->
    <!--                                    <img src="../public/images/aboutus/DrMansor.jpg" alt="Image 1"-->
    <!--                                         class="img-responsive"/></a>-->
    <!--                                <a href="#" class="staff-name">Ybhg. Prof. Dato' Dr. Mansor Bin Fadzil </a>-->
    <!--                                <div class="staff-job">ADVISOR</div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="staff-item customize">-->
    <!--                        <div class="staff-item-wrapper">-->
    <!--                            <div class="staff-info">-->
    <!--                                <a href="#" class="staff-avatar">-->
    <!--                                    <img src="../public/images/aboutus/DrLim.jpg" alt="Image 2" class="img-responsive"/></a>-->
    <!--                                <a href="#" class="staff-name">Prof. Dr. Lim Tick Meng</a>-->
    <!--                                <div class="staff-job">DIRECTOR</div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="staff-item customize">-->
    <!--                        <div class="staff-item-wrapper">-->
    <!--                            <div class="staff-info"><a href="#" class="staff-avatar">-->
    <!--                                    <img src="../public/images/aboutus/Noral.jpg" alt="Image 3" class="img-responsive"/></a>-->
    <!--                                <a href="#" class="staff-name">Noral Hidayah Binti Alwi</a>-->
    <!--                                <div class="staff-job">PROJECT COORDINATOR</div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="staff-item customize">-->
    <!--                        <div class="staff-item-wrapper">-->
    <!--                            <div class="staff-info">-->
    <!--                                <a href="#" class="staff-avatar">-->
    <!--                                    <img src="../public/images/aboutus/Shahrizan.jpg" alt="Image 4"-->
    <!--                                         class="img-responsive"/>-->
    <!--                                </a>-->
    <!--                                <a href="#" class="staff-name">Mohd Shahrizan Bin Sukirman</a>-->
    <!--                                <div class="staff-job">TECHNICAL SUPPORT I</div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="staff-item customize">-->
    <!--                        <div class="staff-item-wrapper">-->
    <!--                            <div class="staff-info">-->
    <!--                                <a href="#" class="staff-avatar">-->
    <!--                                    <img src="../public/images/aboutus/Izral.jpg" alt="Image 4" class="img-responsive"/>-->
    <!--                                </a>-->
    <!--                                <a href="#" class="staff-name">Mohd Izral Bin Mohd Nazir</a>-->
    <!--                                <div class="staff-job">TECHNICAL SUPPORT II</div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="staff-item customize">-->
    <!--                        <div class="staff-item-wrapper">-->
    <!--                            <div class="staff-info">-->
    <!--                                <a href="#" class="staff-avatar">-->
    <!--                                    <img src="../public/images/aboutus/oum.jpg" alt="Image 4" class="img-responsive"/>-->
    <!--                                </a>-->
    <!--                                <a href="#" class="staff-name">All ITLA Staff</a>-->
    <!--                                <div class="staff-job">COMMITTEE</div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="group-btn-slider">-->
    <!--            <div class="btn-prev"><i class="fa fa-angle-left"></i></div>-->
    <!--            <div class="btn-next"><i class="fa fa-angle-right"></i></div>-->
    <!--        </div>-->
    <!--    </div>-->
</div>