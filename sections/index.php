<div class="section section-padding course-section choose-course-2 background-opacity">
    <div class="container">
        <div class="choose-course-wrapper">
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <div class="item-course">
                        <div class="icon-course">
                            <img class="img-position" src="../public/images/customeIcons/ProspectStudent.png">
                        </div>
                        <div class="info-course">
                            <div class="name-course">Prospective Student</div>
                            <div class="info">Begin exploring</div>
                        </div>
                        <div class="hover-text color-86bc42">
                            <div class="wrapper-hover-text">
                                <div class="wrapper-hover-content">
                                    <div class="content">
                                        <div class="title">Prospective Student</div>
                                        <div class="content">
                                            Begin exploring our programs and requirements here.
                                            <br/><br/>
                                            <a href="../prospect" type="button" class="btn btn-primary hvr-pulse-grow">
                                                Learn More
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6">
                    <div class="item-course">
                        <div class="icon-course">
                            <img class="img-position" src="../public/images/customeIcons/NewStudent.png">
                        </div>
                        <div class="info-course">
                            <div class="name-course">New Student</div>
                            <div class="info">Getting started</div>
                        </div>
                        <div class="hover-text color-86bc42">
                            <div class="wrapper-hover-text">
                                <div class="wrapper-hover-content">
                                    <div class="content">
                                        <div class="title">New Student</div>
                                        <div class="content">
                                            Getting started guides for new semester preparation.
                                            <br/><br/>
                                            <a href="../newstudent" type="button" class="btn btn-primary hvr-pulse-grow">
                                                Learn More
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <div class="item-course">
                        <div class="icon-course">
                            <img class="img-position" src="../public/images/customeIcons/ExistingStudent.png">
                        </div>
                        <div class="info-course">
                            <div class="name-course">Senior Student</div>
                            <div class="info">Information and guidance</div>
                        </div>
                        <div class="hover-text color-86bc42">
                            <div class="wrapper-hover-text">
                                <div class="wrapper-hover-content">
                                    <div class="title">Senior Student</div>
                                    <div class="content">
                                        Information and guidance for continuing student.
                                        <br/><br/>
                                        <a href="../existing" type="button" class="btn btn-primary hvr-pulse-grow">
                                            Learn More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6">
                    <div class="item-course">
                        <div class="icon-course">
                            <img class="img-position" src="../public/images/customeIcons/GraduatingStudent.png">
                        </div>
                        <div class="info-course">
                            <div class="name-course">Graduating Student</div>
                            <div class="info">Congratulation!!</div>
                        </div>
                        <div class="hover-text color-86bc42">
                            <div class="wrapper-hover-text">
                                <div class="wrapper-hover-content">
                                    <div class="content">
                                        <div class="title">Graduating Student</div>
                                        <div class="content">
                                            Congratulation!! You're to the end of your time here in OUM.
                                            <br/><br/>
                                            <a href="../graduation" type="button" class="btn btn-primary hvr-pulse-grow">
                                                Learn More
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>