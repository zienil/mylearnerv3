<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper"><!--.page-title-content-->
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">Learning Guide Video</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding gallery-page gallery-4column lv-section background-opacity">
    <div class="container">
        <div class="group-title-index">
            <h2 class="center-title">Learning Guide Video</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-youtube-play" aria-hidden="true"></i></div>
        </div>

        <div class="gallery-nav">
            <ul id="filters" class="nav nav-tabs edugate-tabs button-group filters-button-group">
                <li data-filter="*" data-category="all" class="tab active">
                    <div class="text">all</div>
                </li>
                <li data-filter=".myInspire" data-category="myInspire" class="tab">
                    <div class="text">myInspire</div>
                </li>
                <li id="agLink" data-filter=".ag" data-category="ag" class="tab">
                    <div class="text">Assignment Guide</div>
                </li>
                <li id="sgLink" data-filter=".sg" data-category="sg" class="tab">
                    <div class="text">Survival Guide for New Learners</div>
                </li>
            </ul>
        </div>
        <div class="grid">
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=0I9Ifoabuio" class="link" data-title="Welcoming Remarks by President">
                    <img src="/public/images/videos/sg2.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Welcoming Remarks by President</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=qeD7pTgPr8c" class="link" data-title="Studying at OUM">
                    <img src="/public/images/videos/sg3.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Studying at OUM</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=HP5bWs7mg20" class="link" data-title="Introduction to OUM Digital Library">
                    <img src="/public/images/videos/sg5.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Introduction to OUM Digital Library</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=wYidFuNZxh4" class="link" data-title="Search Printed Books">
                    <img src="/public/images/videos/sg6.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Search Printed Books</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=woQf1-78JPA" class="link" data-title="Post Book to LC">
                    <img src="/public/images/videos/sg7.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Post Book to LC</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=ODHxrZc6fR4" class="link" data-title="Searching eBook">
                    <img src="/public/images/videos/sg8.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Searching eBook</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=u6R2TZgte64" class="link" data-title="Searching Academic Journal">
                    <img src="/public/images/videos/sg9.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Searching Academic Journal</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=EZZPvMqcOIU" class="link" data-title="Search Database Pro Quest">
                    <img src="/public/images/videos/sg10.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Search Database Pro Quest</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=QBw-RovBepQ" class="link" data-title="Semester System">
                    <img src="/public/images/videos/sg11.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Semester System</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=Z4BAzt7lPrI" class="link" data-title="Academic Rules">
                    <img src="/public/images/videos/sg12.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Academic Rules</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=KTWdfoTUDOc" class="link" data-title="eCRM">
                    <img src="/public/images/videos/sg13.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>eCRM</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=AnLYPsmiBts" class="link" data-title="Financial Support">
                    <img src="/public/images/videos/sg14.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Financial Support</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="sg" class="grid-item sg">
                <a href="https://www.youtube.com/watch?v=ja-n5qUNRi8" class="link" data-title="EVERYBODY DIES, BUT NOT EVERYBODY LIVES">
                    <img src="/public/images/videos/sg15.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Survival Guide</span>-->
                    <span class="content-edu">
                        <span><strong>EVERYBODY DIES, BUT NOT EVERYBODY LIVES</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=73naQiwNavw" class="link" data-title="Accessing myInspire App">
                    <img src="/public/images/videos/myInspireApp.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Accessing myInspire App</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=JSXkGHkX3ck" class="link"
                   data-title="Writing the Content body of your assignment">
                    <img src="/public/images/videos/ag1.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Writing the Content body of your assignment</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=j92XG40oevo" class="link"
                   data-title="Accessing via mobile website">
                    <img src="/public/images/videos/myInspireMobileWeb.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Accessing via mobile website</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=eX_Yug2fwdc" class="link"
                   data-title="Accessing via myInspire website">
                    <img src="/public/images/videos/myInspireWebsite.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Accessing via myInspire website</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=sLm97i9XIHg" class="link"
                   data-title="Accessing via myOUM Portal">
                    <img src="/public/images/videos/myInspireOUM.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Accessing via myOUM Portal</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=Y1wYSKCIILc" class="link"
                   data-title="Downloading myInspire App">
                    <img src="/public/images/videos/myInspireDLApp.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Downloading myInspire App</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=pDwNyLizHTc" class="link" data-title="Citations">
                    <img src="/public/images/videos/ag2.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Citations</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=tG27tzEZA7U" class="link"
                   data-title="Viewing Course Module">
                    <img src="/public/images/videos/myInspireViewCourse.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Viewing Course Module</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=GIGTFrCHmp8" class="link"
                   data-title="Understanding your assignment rubric">
                    <img src="/public/images/videos/ag3.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Understanding your assignment rubric</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=CxvtxwIyUts" class="link"
                   data-title="Viewing the Course Forum">
                    <img src="/public/images/videos/myInspireViewCourseModule.png"
                         class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Viewing the Course Forum</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=0lY_jotGPFA" class="link"
                   data-title="Viewing Video Lectures">
                    <img src="/public/images/videos/myInspireVideoLecturer.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Viewing Video Lectures</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=DBeKxxR5RY0" class="link"
                   data-title="Knowing myInspire CoursePage">
                    <img src="/public/images/videos/myInspireCoursePage.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Knowing myInspire CoursePage</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=6ugQgZ3Kb5Y" class="link" data-title="Good and bad assignment">
                    <img src="/public/images/videos/ag4.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Good and bad assignment</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=EypUUDE0DjA" class="link"
                   data-title="Using Search Forum Feature">
                    <img src="/public/images/videos/myInspireSearchForum.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Using Search Forum Feature</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=NtBfHIWkI20" class="link"
                   data-title="Posting New Message in Forum">
                    <img src="/public/images/videos/myInspireMsgForum.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Posting New Message in Forum</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=ZgkRY5gEfO4" class="link"
                   data-title="Understanding and avoiding plagiarism">
                    <img src="/public/images/videos/ag5.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Understanding and avoiding plagiarism</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=Qd6VpzfbdQU" class="link"
                   data-title="Replying to Message in Forum">
                    <img src="/public/images/videos/myInspireReplayMsg.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Replying to Message in Forum</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=xhApGikZ60U" class="link"
                   data-title="Summarizing, Paraphrasing and Quoting in your assignment">
                    <img src="/public/images/videos/ag6.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Summarizing, Paraphrasing and Quoting in your assignment</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=VyhFh79gfH8" class="link"
                   data-title="Downloading Module by Topic">
                    <img src="/public/images/videos/myInspireDownloadModule.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Downloading Module by Topic</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=xNCjjjslw5g" class="link"
                   data-title="Downloading Submitting Assignment">
                    <img src="/public/images/videos/myInspireDownloadAssg.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Downloading Submitting Assignment</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=6L2ND1b_2rk" class="link"
                   data-title="Understanding your assignment question">
                    <img src="/public/images/videos/ag7.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Understanding your assignment question</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="ag" class="grid-item ag">
                <a href="https://www.youtube.com/watch?v=Pzz7EEFDO6M" class="link"
                   data-title="Writing the Introduction of your assignment">
                    <img src="/public/images/videos/ag8.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">Assignment Guide</span>-->
                    <span class="content-edu">
                        <span><strong>Writing the Introduction of your assignment</strong></span>
                    </span>
                </span>
            </div>
            <div data-category="myInspire" class="grid-item myInspire">
                <a href="https://www.youtube.com/watch?v=fpyaqm5MCwM" class="link"
                   data-title="Downloading eModule">
                    <img src="/public/images/videos/myInspireDownloadeModule.png" class="img-responsive img-thumbnail"/>
                </a>
                <span class="content">
<!--                    <span class="content-title">MyInspire</span>-->
                    <span class="content-edu">
                        <span><strong>Downloading eModule</strong></span>
                    </span>
                </span>
            </div>
        </div>
    </div>
</div>