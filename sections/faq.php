<div class="section page-title set-height-top">
    <div class="container">
        <div class="page-title-wrapper">
            <ol class="breadcrumb">
                <li><a href="../index">Home</a></li>
                <li class="active"><a href="#">FAQ</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="section section-padding faq-section background-opacity">
    <div class="container">
        <div class="group-title-index">
            <h4 class="top-title">Answer all of your questions</h4>
            <h2 class="center-title">FREQUENTLY ASKED QUESTIONS</h2>
            <div class="bottom-title"><i class="bottom-icon fa fa-commenting-o"></i></div>
        </div>
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified faq-cat-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#faq1" aria-controls="faq1" role="tab" data-toggle="tab">ENTRY REQUIREMENT</a>
                </li>
                <li role="presentation">
                    <a href="#faq2" aria-controls="faq2" role="tab" data-toggle="tab">FINANCE</a>
                </li>
                <li role="presentation">
                    <a href="#faq3" aria-controls="faq3" role="tab" data-toggle="tab">ASSESSMENT</a>
                </li>
                <li role="presentation">
                    <a href="#faq4" aria-controls="faq4" role="tab" data-toggle="tab">RULES AND REGULATIONS</a>
                </li>
                <li role="presentation">
                    <a href="#faq5" aria-controls="faq5" role="tab" data-toggle="tab">TEACHING & LEARNING</a>
                </li>
                <li role="presentation">
                    <a href="#faq6" aria-controls="faq6" role="tab" data-toggle="tab">SYSTEM SUPPORT</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <br/>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="faq1">
                    <div class="accordion-faq">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="underline">Entry Requirement</div>
                                <div id="accordion-1" class="panel-group accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-1"
                                                   href="#collapse-1-1"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    1. How do I apply?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-1-1" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Walk-in to any of our learning centers or main campus.</li>
                                                    <li>Fill in the Online Application Form which is available at OUM
                                                        Website.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-1"
                                                   href="#collapse-1-2"
                                                   aria-expanded="true" class="accordion-toggle collapsed">
                                                    2. How do I know if my application is successful?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-1-2" style="" aria-expanded="true"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Online applicants will be given an immediate conditional letter
                                                        of admission.
                                                    </li>
                                                    <li>
                                                        Confirmation of OUM student status depends on the submission of
                                                        evidence for entry qualifications.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading"><h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-1"
                                                   href="#collapse-1-3"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    3. What happens if I can't attend on the registration day?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-1-3" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        OUM practices open registration. You can register anytime on the
                                                        date stipulated in your letter of offer. If you cannot register
                                                        for the semester, you may register on the following semester.
                                                        The letter of offer is valid from the date of offer.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-1"
                                                   href="#collapse-1-4"
                                                   aria-expanded="true" class="accordion-toggle collapsed">
                                                    4. What is the minimum fee required to register?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-1-4" style="" aria-expanded="true"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Undergraduate <span class="label label-primary">RM400</span>
                                                    </li>
                                                    <li>
                                                        Postgraduate <span class="label label-primary">RM1000</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-1"
                                                   href="#collapse-1-5"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    5. Where do I register?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-1-5" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>At the learning centers specified in the letter of offer.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-1"
                                                   href="#collapse-1-6"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    6. What do I need to bring during registration?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-1-6" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ol class="text-answer">
                                                    <li>National Identification Card (NRIC)</li>
                                                    <li>Conditional Letter of admission</li>
                                                    <li>Completed Declaration Sheet</li>
                                                    <li>Original AND a copy of RECOGNISED academic certificate/s</li>
                                                    <li>The verification of recognised certification/s printed from
                                                        MQA/JPA website
                                                    </li>
                                                    <li>Original AND a copy of Identification Card / Passport</li>
                                                    <li>Original AND a copy of Job Confirmation Letter, if stated in the
                                                        programme entry requirement
                                                    </li>
                                                    <li>Two copies of completed UKR 19 Form</li>
                                                    <li>Original bank-in slip (Student Copy) or any other proof of
                                                        payment
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="faq2">
                    <div class="accordion-faq">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="underline">Finance</div>
                                <div class="panel panel-default" style="position: relative;z-index: 99;">
                                    <div class="panel-body">
                                        <p class="text-warning">
                                            Finance is a major concern when one is contemplating further studies. At
                                            OUM, we want to assure you that there are various channels to turn to so
                                            that paying for your studies does not become a burden. The following are
                                            some frequently asked questions about payment modes and sources of
                                            financing.
                                        </p>
                                    </div>
                                </div>
                                <div id="accordion-2" class="panel-group accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-1"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    1. What are the methods of payment?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-1" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <tbody>
                                                        <tr>
                                                            <th rowspan="2" scope="row" class="warning text-danger"
                                                                style="vertical-align: middle">
                                                                Direct Payment
                                                            </th>
                                                            <td class="success text-primary">
                                                                Bank Islam Malaysia Berhad
                                                                <ul>
                                                                    <li>Payment made to UNITEM</li>
                                                                    <li>Account No: 14041-01-0053385</li>
                                                                    <li>Payment slip can be obtained from Bank Islam
                                                                        branches
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="success text-primary">
                                                                GIRO Bank Simpanan Nasional
                                                                <ul>
                                                                    <li>Payment made to UNIVERSITI TERBUKA MALAYSIA</li>
                                                                    <li>Account No: 14100-29-00022495-2</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th rowspan="5" scope="row" class="warning text-danger"
                                                                style="vertical-align: middle">
                                                                Internet Banking
                                                            </th>
                                                            <td class="success text-primary">
                                                                <a href="http://www.cimbclicks.com.my/" target="_blank"
                                                                   class="text-danger">CIMB Clicks</a> for CIMB account
                                                                holder > Pay Bills
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="success text-primary">
                                                                Financial Payment Exchange (FPX)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="success text-primary">
                                                                <a href="http:// www.maybank2u.com.my" target="_blank"
                                                                   class="text-danger">MayBank2U</a> for Maybank account
                                                                holder >
                                                                Pay Bills
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="success text-primary">
                                                                http://www.onlineapps8.oum.edu.my/StudentPortal/onpayment_new.jsp
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="success text-primary">
                                                                <a href="https://www.bankislam.biz/" target="_blank"
                                                                   class="text-danger">Bankislam.biz</a> for BIMB
                                                                account holder
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Credit card</th>
                                                            <td class="success text-primary">Mastercard or Visa (at
                                                                OUM’s Main Campus and all Regional Learning Centres)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">e-Debit</th>
                                                            <td class="success text-primary">Mastercard or Visa (at
                                                                OUM’s Available at OUM KL and all Learning Centers
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Cheque/ postal
                                                                order/ money order
                                                            </th>
                                                            <td class="success text-primary">
                                                                <ul>
                                                                    <li>Payee: UNIVERSITI TERBUKA MALAYSIA</li>
                                                                    <li>A penalty of RM50 will be imposed on any
                                                                        dishonoured cheques.
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Cash</th>
                                                            <td class="success text-primary">
                                                                Payment can be made at OUM Learning Centers
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Employee’s
                                                                Provident Fund (EPF)
                                                            </th>
                                                            <td class="success text-primary">
                                                                Learners can withdraw their EPF contributions from
                                                                Account II to pay their fees. The form to apply for
                                                                withdrawal of contribution can be obtained from any EPF
                                                                branch.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Other
                                                                Educational Loans
                                                            </th>
                                                            <td class="success text-primary">
                                                                Learners can obtain loan from any institutions which
                                                                offer educational loans such as banks, state
                                                                foundations, and educational funds and so on. Once the
                                                                loan is secured, learners must forward a copy of the
                                                                Agreement and the Letter of Award to the Accounts
                                                                Department
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <ol class="text-answer">
                                                    <li>Direct Payment.</li>
                                                    <ul>
                                                        <li>Bank Islam Malaysia Berhad is made under the name
                                                            UNITEM Account No: 14041-01-0053385 using OUM own
                                                            pre-printed Deposit slip which can be obtained at the
                                                            Bank Islam branches;
                                                        </li>
                                                    </ul>
                                                    <li>Internet Banking</li>
                                                    <ul>
                                                        <li>
                                                            <a href="http://www.cimbclicks.com.my/" target="_blank"
                                                               class="text-danger">CIMB Clicks</a> for CIMB account
                                                            holder > Pay Bills
                                                        </li>
                                                        <li>
                                                            Financial Payment Exchange (FPX) -
                                                            http://www.onlineapps8.oum.edu.my/StudentPortal/
                                                            onpayment_new.jsp
                                                        </li>
                                                        <li>
                                                            <a href="https://www.bankislam.biz/" target="_blank"
                                                               class="text-danger">Bankislam.biz</a> for BIMB account
                                                            holder
                                                        </li>
                                                    </ul>
                                                    <span class="text-warning">
                                                        <b>Note : Any transaction via Cash Deposit Machine (CDM) and Fund Transfer are not acceptable</b>
                                                    </span>
                                                    <li>Credit Card - Mastercard or Visa (at OUM’s Main Campus and all
                                                        Regional Learning Centres)
                                                    </li>
                                                    <li>E-Debit - Available at OUM KL and all Learning Centers</li>
                                                    <li>Cheque/ postal order/ money order - A penalty of RM50 will be
                                                        imposed on any dishonoured cheques.
                                                    </li>
                                                    <li>Cash at OUM Learning Centers.</li>
                                                    <li>Employee’s Provident Fund (EPF)</li>
                                                    <ul>
                                                        <li>Learners can withdraw their EPF contributions fromAccount II
                                                            to pay their fees. The form for the application for
                                                            withdrawal of contribution can be obtained from any EPF
                                                            branch.
                                                        </li>
                                                        <li>Learners will be required to obtain the Confirmation
                                                            Letter from the Learner Service Centre (1-300-88- 7300) to
                                                            complete the application. However, take note that PJJKPM-OUM
                                                            learners are not allowed to withdraw their EPF
                                                            contributions.
                                                        </li>
                                                    </ul>
                                                    <li>Other Educational Loans</li>
                                                    <ul>
                                                        <li>Learners can obtain loan from any institutions which offer
                                                            educational loans such as banks, state foundations, and
                                                            educational funds and so on. Once the loan is secured,
                                                            learners must forward a copy of the Agreement and the Letter
                                                            of Award to the Accounts Department
                                                        </li>
                                                    </ul>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-2"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    2. When must I settle outstanding tuition fees balance?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-2" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Any outstanding tuition fees must be settled one week before the
                                                        examination and the proof of payment such as Bank-in Slip must
                                                        be sent to the nearest Learning Centre or fax to 03-2697 8815
                                                    </li>
                                                    <li>
                                                        In the event where you fail to settle the payment for the
                                                        semester, your examination results for the semester will not be
                                                        released in your Student Profile.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-3" aria-expanded="false"
                                                   class="accordion-toggle collapsed">
                                                    3. How much must I pay for re-registration?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-3" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Minimum payment for every semester is RM400.00, plus outstanding
                                                        balance from the previous semester
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-4" aria-expanded="false"
                                                   class="accordion-toggle collapsed">
                                                    4. Can I obtain a refund of the fees if I have outstanding balance?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-4" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Yes. The form can be obtained from OUM Portal > eServices >
                                                        Finance > Refund fee application form
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-5"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    5. Where can I turn to for financial assistance?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-5" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        <b>EPF withdrawals :</b>
                                                        <ul>
                                                            <li>EPF withdrawals can only be made after the initial
                                                                minimum payments for registration and the first semester
                                                                are made.
                                                            </li>
                                                            <li>Withdrawal supporting documents can be requested from
                                                                the Learner Service Centre at 1300 887 300.
                                                            </li>
                                                            <li>EPF members with i-Akaun may submit Online Application
                                                                via KWSP Portal
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <b>PTPTN loans : </b>
                                                        <ul>
                                                            <li>This applies only for direct entry undergraduate
                                                                programmes.
                                                            </li>
                                                            <li>Covering 80% of the total course fees, payback will
                                                                commence after completion of studies at 1% per annum
                                                                interest rate.
                                                            </li>
                                                            <li>Steps to apply:</li>
                                                            <ul>
                                                                <li>PTPTN applicant must have already received OUM´s
                                                                    offer letter prior to applying
                                                                </li>
                                                                <li>Buy a PIN number from Bank Simpanan Nasional (RM5).
                                                                    This PIN number will be valid for 6 months from the
                                                                    purchase date.
                                                                </li>
                                                                <li> Fill up the online application form at PTPTN
                                                                    website
                                                                </li>
                                                                <li>Check for application results six (6) working days
                                                                    after the application closing date from the PTPTN
                                                                    website.
                                                                </li>
                                                                <li>Print out all required documents and submit the
                                                                    documents to OUM for processing.
                                                                </li>
                                                                <li>For the full list of PTPTN requirements, kindly
                                                                    refer to PTPTN website.
                                                                </li>
                                                            </ul>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <b>Human Resource Development Fund claims</b>
                                                        <ul>
                                                            <li>The student or company will pay for the fees first.
                                                                Claims must then be submitted to the HRDF on a yearly
                                                                basis. This is on condition that the company is
                                                                contributing to the HRDF; the amount claimable is
                                                                limited to the amount of contribution.
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <b>Yayasan Canselor OUM</b>
                                                        <ul>
                                                            <li>
                                                                There are several bursary schemes under Yayasan Canselor
                                                                OUM. These schemes are open to Malaysian citizens
                                                                registered with OUM, as follows:
                                                                <ol>
                                                                    <li>Senior students:
                                                                        <ul>
                                                                            <li>Enrolled in any Diploma or Bachelor
                                                                                level programme
                                                                            </li>
                                                                            <li>Existing learners who are in the 2nd,
                                                                                3rd and 4th year of study.
                                                                            </li>
                                                                            <li>Maintains a GPA of not less than 3.70
                                                                                for each semester throughout the Bursary
                                                                                eligibility period.
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>New students:
                                                                        <ul>
                                                                            <li>
                                                                                Enrolled in any Diploma or Bachelor
                                                                                level programme
                                                                            </li>
                                                                            <li>
                                                                                New learners with excellent entrance
                                                                                qualifications with relevant financial
                                                                                background. Single mothers are
                                                                                encouraged to apply.
                                                                            </li>
                                                                            <li>Enrolled in OUM for not less than 1
                                                                                academic year
                                                                            </li>
                                                                            <li>Maintains a GPA of not less than 3.70
                                                                                for each semester throughout the Bursary
                                                                                eligibility period.
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>Postgraduate Students:
                                                                        <ul>
                                                                            <li>Enrolled in any Master’s programme, EdD,
                                                                                DBA or Doctor of Nursing
                                                                            </li>
                                                                            <li>Existing learners with excellent
                                                                                academic performance and they are in the
                                                                                2nd year of study in OUM.
                                                                            </li>
                                                                            <li>Maintains a GPA of not less than 3.80
                                                                                (Master) or 3.90 (EdD, DBA & DN) for
                                                                                each semester throughout the Bursary
                                                                                eligibility period.
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>Alumni:
                                                                        <ul>
                                                                            <li>Enrolled in any Master’s programme, EdD,
                                                                                DBA or Doctor of Nursing
                                                                            </li>
                                                                            <li>The bursary is awarded to all OUM Alumni
                                                                                members who obtained a First Class
                                                                                Degree.
                                                                            </li>
                                                                            <li>Maintains a GPA of not less than 3.80
                                                                                (Master) or 3.90 (EdD, DBA & DN) for
                                                                                each semester throughout the Bursary
                                                                                eligibility period.
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>Incentives for Athletes and Coaches:
                                                                        <ul>
                                                                            <li>Enrolled in any Diploma or Bachelor
                                                                                level programme
                                                                            </li>
                                                                            <li>The incentives are opened to athletes
                                                                                and trainers with excellent
                                                                                accomplishment in sports and are active
                                                                                for the past 5 years.
                                                                            </li>
                                                                            <li>Maintains a GPA of not less than 2.00
                                                                                for each semester throughout the Bursary
                                                                                eligibility period.
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>Special Bursary:
                                                                        <ul>
                                                                            <li>Enrolled in any Diploma or Bachelor
                                                                                level programme
                                                                            </li>
                                                                            <li>New learners with special entrance
                                                                                qualification(s) with relevant financial
                                                                                background.
                                                                            </li>
                                                                            <li>Maintains a GPA of not less than 2.00
                                                                                for each semester throughout the Bursary
                                                                                eligibility period.
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-6"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    6. Who can apply for PTPTN loan?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-6" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Applicable for Undergraduate programmes.</li>
                                                    <li>Malaysian citizen</li>
                                                    <li>Not more than 45 years of age on the date of application</li>
                                                </ul>
                                                <p class="text-danger">
                                                    Note : Students must maintain a GPA of 2.00 in any semester in order
                                                    to be eligible for the loan for the semester.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-7"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    7.When should I apply for PTPTN?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-7" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-condensed table-hover">
                                                        <thead>
                                                        <tr class="danger text-warning">
                                                            <th></th>
                                                            <th colspan="2">Range 1</th>
                                                            <th colspan="2">Range 2</th>
                                                            <th colspan="2">Range 3</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row" class="warning text-answer">Application
                                                                Period
                                                            </th>
                                                            <td class="text-primary">1 Jan – 31 Jan</td>
                                                            <td class="text-primary">1 Feb – 28/29 Feb</td>
                                                            <td class="text-primary">1 Jun – 30 Jun</td>
                                                            <td class="text-primary">1 Jul – 31 Jul</td>
                                                            <td class="text-primary">1 Oct – 31 Oct</td>
                                                            <td class="text-primary">1 Nov – 30 Nov</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-answer">Closing date
                                                            </th>
                                                            <td class="text-primary">31 Jan</td>
                                                            <td class="text-primary">28/29 Feb</td>
                                                            <td class="text-primary">30 Jun</td>
                                                            <td class="text-primary">31 Jul</td>
                                                            <td class="text-primary">31 Oct</td>
                                                            <td class="text-primary">30 Nov</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-answer">Approval date
                                                            </th>
                                                            <td class="text-primary">7 Feb</td>
                                                            <td class="text-primary">7 Mar</td>
                                                            <td class="text-primary">7 July</td>
                                                            <td class="text-primary">7 August</td>
                                                            <td class="text-primary">7 Nov</td>
                                                            <td class="text-primary">7 Dec</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-8"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    8. When is the closing date to submit the PTPTN Agreement &
                                                    Supporting
                                                    documents to OUM?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-8" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Within 14 days from the approval date.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-9"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    9. How long will the process take until the release of my PTPTN
                                                    loan?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-9" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>1 - 2 months</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-10"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    10. Am I eligible for EPF withdrawal?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-10" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Yes, you may withdraw from Account 2. However, contributors are
                                                        subject to EPF regulations.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-11"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    11. How do I apply for EPF withdrawal?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-11" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Request for ‘Surat Pengesahan Pelajar’ from OUM’s Learner
                                                        Services Centre.
                                                    </li>
                                                    <li>Fill up the application form from EPF.</li>
                                                    <li>Submit the application form together with the above letter to
                                                        EPF.
                                                    </li>
                                                    <li>EPF members with i-Akaun may submit Online Application via KWSP
                                                        Portal
                                                        <ul>
                                                            <li>EPF member will be notified by SMS and i-Akaun Secured
                                                                Inbox Messaging
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-12"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    12. Why am I not able to access my registered courses in myINSPIRE
                                                    to print my module although I have paid all the fees?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-12" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Your registered courses will be updated in myINSPIRE three (3)
                                                        working days after you have made the minimum payment of
                                                        registration for the Current Semester. For undergraduate
                                                        programs, the minimum fee is RM400 per semester.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-13"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    13. Why are my registered subjects not displayed in myINSPIRE?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-13" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Courses in myINSPIRE will be updated if the student has made a
                                                        complete registration, which means that students have submitted
                                                        their registration confirmation and have made the minimum
                                                        payment.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-14"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    14. When can I see the updated financial statement for the new
                                                    semester?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-14" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Financial statement will only be updated 14 days after the
                                                        students submit proof of the transaction / payment.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-15"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    15. How can I submit a claim for excess fees paid?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-15" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Students can reclaim the excess payment refund by completing the
                                                        form provided in OUM Portal
                                                        <ul>
                                                            <li>OUM Portal > eServices > Finance > Fee
                                                                Refund Application Form
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        Please complete the form and return theform by fax or mail to
                                                        the fax number or email address listed on the form. The refund
                                                        of fees will be made within 7 working days.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-16"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    16. What are the different type of fees that student need to be
                                                    aware of?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-16" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Type of fees :</li>
                                                </ul>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <thead class="text-warning">
                                                        <tr class="danger">
                                                            <th colspan="2" class="text-center text-uppercase">
                                                                Examination fees
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-primary">
                                                        <tr>
                                                            <td>Re-sit examination</td>
                                                            <td>RM100 / subject</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Remarking Fee</td>
                                                            <td>RM50 / subject</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Change of exam location
                                                                <ul>
                                                                    <li>After official closing date</li>
                                                                    <li>Without application</li>
                                                                </ul>
                                                            </td>
                                                            <td>RM10 / subject
                                                                <ul>
                                                                    <li>RM50 / subject</li>
                                                                    <li>RM100 / subject</li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                        <thead class="text-warning">
                                                        <tr class="danger">
                                                            <th colspan="2" class="text-center text-uppercase">Credit
                                                                transfer
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-primary">
                                                        <tr>
                                                            <td>Undergraduate / Postgraduate courses</td>
                                                            <td>RM30 / credit hour</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nursing Core courses</td>
                                                            <td>RM50 / credit hour</td>
                                                        </tr>
                                                        </tbody>
                                                        <thead class="text-warning">
                                                        <tr class="danger">
                                                            <th colspan="2" class="text-center text-uppercase">Others
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-primary">
                                                        <tr>
                                                            <td>Change of programme</td>
                                                            <td>RM50 / application</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Late registration (after T1)</td>
                                                            <td>RM100</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Active fee (Not register & Defer)</td>
                                                            <td>RM50 / semester</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matric Card replacement</td>
                                                            <td>RM15 / card</td>
                                                        </tr>
                                                        </tbody>
                                                        <thead class="text-warning">
                                                        <tr class="danger">
                                                            <th colspan="2" class="text-center text-uppercase">
                                                                Withdrawal from the course
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-primary">
                                                        <tr>
                                                            <td>Before T1</td>
                                                            <td>No charges</td>
                                                        </tr>
                                                        <tr>
                                                            <td>After T1</td>
                                                            <td>Full fees for the semester except exam fees</td>
                                                        </tr>
                                                        </tbody>
                                                        <thead class="text-warning">
                                                        <tr class="danger">
                                                            <th colspan="2" class="text-center text-uppercase">
                                                                Withdrawal from the University
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="text-primary">
                                                        <tr>
                                                            <td>Before T1</td>
                                                            <td>Non-recurring fees (new student), OR Recurring fees
                                                                (senior student).<br/>
                                                                <i>Note: Excess fees (if any) will be refunded.</i>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Between T1 to T2</td>
                                                            <td>Non-recurring fees Recurring fees , AND Tuition fees
                                                                (50%).<br/>
                                                                <i>Note: Excess fees (if any) will be refunded.</i>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>After T2 until First Day of Examination</td>
                                                            <td>Full fees for the semester except exam fees</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2"
                                                   href="#collapse-2-17"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    17. Can I make any amendment or correction with regards to my
                                                    personal information in the certificate?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-2-17" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Before convocation:
                                                        <ul>
                                                            <li>
                                                                Any changes in name, address, telephone number and email
                                                                can be done in OUM Portal > myProfile > Edit > Save.
                                                            </li>
                                                            <li>
                                                                Other changes need to be submitted to Admission and
                                                                Records Unit via fax 03- 2697 8794. Please attach a copy
                                                                of your IC for our reference
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>After convocation:
                                                        <ul>
                                                            <li>
                                                                You need to contact the Assessment and Examination
                                                                Department at 03- 2773 2626.
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>Changes can be made for the following:
                                                        <ol>
                                                            <li>Scroll
                                                                <ol type="a">
                                                                    <li>Programme</li>
                                                                    <li>Name</li>
                                                                </ol>
                                                            </li>
                                                            <li> Transcript
                                                                <ol type="a">
                                                                    <li>Programme</li>
                                                                    <li>Name</li>
                                                                    <li>Major</li>
                                                                    <li>Grade</li>
                                                                    <li>GPA / CGPA</li>
                                                                    <li>Nationality</li>
                                                                    <li>Gender</li>
                                                                </ol>
                                                            </li>
                                                            <li>Others
                                                                <ol type="i">
                                                                    <li>Changes in spelling of name will be charged as
                                                                        follows:
                                                                        <ul>
                                                                            <li>Scroll: RM20</li>
                                                                            <li> Transcript: RM10</li>
                                                                            <li>Delivery charges: RM10</li>
                                                                        </ul>
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="faq3">
                    <div class="accordion-faq">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="underline">Assessment (Assignment & Examination)</div>
                                <div id="accordion-3" class="panel-group accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-1"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    1. What are the types of assessment given to learners?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-1" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Method 1</th>
                                                            <td class="text-primary">100% Assignment</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Method 2</th>
                                                            <td class="text-primary">Assgnment + Final exam</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <p class="text-danger">
                                                    <strong>*Different courses may have different types of
                                                        Assessment</strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-2"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    2. Where can I retrieve my assignment question?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-2" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">
                                                        Assignment questions will be uploaded in myInspire according to
                                                        the subject enrolled for the semester.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-3"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    3. It is hard for me to understand the requirement of the assignment
                                                    given. Is there any help that I can refer to?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-3" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Yes, you can discuss with your Face-to-face
                                                        tutor and also your e-tutor
                                                    </li>
                                                    <li class="text-answer">You can watch the video at Learning Guide
                                                        Video > Assignment Guide
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-4"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    4. Where should I submit my assignment?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-4" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">There are 2 submission modes:</li>
                                                </ul>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Online
                                                                submission
                                                            </th>
                                                            <td class="text-primary">OUM Portal > myInspire > Assignment
                                                                > Assignment Submission > Upload file > Submit
                                                                assignment
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th rowspan="2" scope="row" class="warning text-danger">
                                                                Face-to-face submission
                                                            </th>
                                                            <td class="text-primary">
                                                                <ul>
                                                                    <li>Face-to-face learners:
                                                                        <ul>
                                                                            <li>
                                                                                Submit the assignment to the
                                                                                Face-to-face tutor/facilitator and
                                                                                collect the assignment submission
                                                                                receipt
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-primary">
                                                                <ul>
                                                                    <li>Online learners:
                                                                        <ul>
                                                                            <li>
                                                                                Refer to faculty announcement for
                                                                                submission mode > collect the assignment
                                                                                submission receipt
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-5"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    5. When will my assignment result be released?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-5" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Estimated to be after T4</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-6"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    6. When and where are my examinations held?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-6" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Details of examination will be announced in
                                                        the website (Exam Announcement) one month before the beginning
                                                        of each examination session.
                                                    </li>
                                                    <li class="text-answer">Learners are advised to check the
                                                        announcement section regularly for the latest updates on the
                                                        location of the examination.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-7"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    7. Can I apply to change my Examination Centre?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-7" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Yes.</li>
                                                    <li class="text-answer">Complete and submit the UP-10 form to
                                                        Assessment & Examination Department after the final examination
                                                        timetable is released.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-8"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    8. Where do I get my Examination Slip?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-8" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Via the online portal.</li>
                                                    <li class="text-answer">Click at e-Services > myProfile >
                                                        Examination > Examination slip
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-9"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    9. Is there any supporting letter as proof to my employer for exam
                                                    leave application?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-9" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Yes</li>
                                                    <li class="text-answer">
                                                        Undergraduates:
                                                        <ul>
                                                            <li class="text-answer">Via the online portal.</li>
                                                            <li class="text-answer">Click at e-Services >
                                                                Undergraduates
                                                                Services & Support > Exam Online > Slip
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="text-answer">
                                                        Postgraduates:
                                                        <ul>
                                                            <li class="text-answer">Via the online portal.</li>
                                                            <li class="text-answer">Click at e-Services > Postgraduates
                                                                (Services & Support)> Exam Online > Final Semester
                                                                Examination Sitting Permission Slip(UP 09)
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-10"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    10. When will my Examination Results be released?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-10" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul>
                                                    <li class="text-answer">Six weeks from the last day of the
                                                        examination.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-11"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    11. How do I appeal for my grade?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-11" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Appeal must be made within 14 days after official announcement
                                                        of the examination results on the announcement.
                                                    </li>
                                                    <li>Undergraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Complete and submit the Appeal Form (UP-03) via online at
                                                            e-Services > Undergraduates Services & Support > Exam Online
                                                            > Online Application > Rayuan Semakan Semula Gred
                                                            Peperiksaan Akhir (UP-03).
                                                        </li>
                                                        <li>Enclose RM50.00 fee or provide documentary proof of payment.
                                                        </li>
                                                        <li>Payment to be made payable to UNITEM SDN BHD</li>
                                                    </ul>
                                                    <li>Postgraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Complete and submit the Recheck Examination (UP03) via
                                                            online. Click at e-Services > Postgraduates (Services &
                                                            Support)> Exam Online > Online Application > Recheck
                                                            Examination (UP03).
                                                        </li>
                                                        <li>Enclose RM100.00 fee or provide documentary proof of
                                                            payment.
                                                        </li>
                                                        <li>Payment to be made payable to UNITEM SDN BHD</li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-12"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    12. Can I re-sit for my paper?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-12" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Yes. However you are required to fill in the application form
                                                    </li>
                                                    <li>Undergraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Via the online portal. Click at e-Services > Undergraduates
                                                            Services & Support > Exam Online > Online Application Form >
                                                            Permohonan Mengulang Peperiksaan Akhir atau Menyempurnakan
                                                            Status I (UP-11)
                                                        </li>
                                                    </ul>
                                                    <li>Postgraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Via the online portal. Click at e-Services > Postgraduates
                                                            (Services & Support) > Exam Online > Online Application Form
                                                            > Application for Refer Final Exam or Completing I Status
                                                            Semester (UP 11)
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-13"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    13. How can I complete my “I” (Incomplete) status?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-13" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>You are required to fill in the application form</li>
                                                    <li>Undergraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Via the online portal. Click at e-Services > Undergraduates
                                                            Services & Support > Exam Online > Online Application Form >
                                                            Permohonan Mengulang Peperiksaan Akhir atau Menyempurnakan
                                                            Status I (UP-11)
                                                        </li>
                                                    </ul>
                                                    <li>Postgraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Via the online portal. Click at e-Services > Postgraduates
                                                            (Services & Support) > Exam Online > Online Application Form
                                                            > Application for Refer Final Exam or Completing I Status
                                                            Semester (UP 11)
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-14"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    14. When can I re-sit for my paper?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-14" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>In any semester when the subject is offered.</li>
                                                    <li>Within a year from the first time you sit for the paper</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-15"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    15. How to postpone my final exam?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-15" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>You are required to fill in the application form</li>
                                                    <li>Undergraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Via the online portal. Click at e-Services > Undergraduates
                                                            Services & Support > Exam Online > Online Application Form >
                                                            Permohonan Penangguhan Peperiksaan Akhir (UP-17)
                                                        </li>
                                                    </ul>
                                                    <li>Postgraduates :</li>
                                                    <ul>
                                                        <li>
                                                            Via the online portal. Click at e-Services > Postgraduates
                                                            (Services & Support) > Exam Online > Online Application Form
                                                            > Application for Postponment of Final Exam (UP 17)
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-16"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    16. I have completed my course. How do I check whether I am eligible
                                                    for convocation?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-16" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Check qualification :</li>
                                                    <ul>
                                                        <li>
                                                            Via online portal > myProfile > Academic Progress > Total
                                                            credit to be completed equal to 0
                                                        </li>
                                                    </ul>
                                                    <li>Check status :</li>
                                                    <ul>
                                                        <li>
                                                            OUM website > Convocation > Semakan Graduan > Insert IC
                                                            number > Status
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-17"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    17. Where can I get the letter of completion for my study?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-17" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Request from ECRM.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-18"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    18. I need a transcript certified by the Open University Malaysia.
                                                    Can I request for the transcript to be sent to my employer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-18" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Yes, OUM will post the printed transcript to any given address
                                                        as requested by the student.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-19"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    19. When will I be able to receive my transcript and letter of
                                                    completion of study?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-19" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Letter of graduation will be processed within two weeks from the
                                                        date of application. The letter will be posted via regular mail
                                                        and delivery time depends on the delivery address.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-20"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    20. Is there any exemption to apply for credit transfers for MPU
                                                    subjects?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-20" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Exemption will not be granted in this situation. This is because
                                                        when a student is enrolled in a private higher learning
                                                        institution in Malaysia, a student is bound by the Private
                                                        Higher Education Act 1996 (Act 555) and the student is required
                                                        to take MPU subjects.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-21"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    21. What should I do when there is an overlap of different
                                                    examination papers at the same time in my exam schedule?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-21" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>You can lodge a complaint in the eCRM for the Examination Unit
                                                        to rectify the issue.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-22"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    22. Can I change my exam location?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-22" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Yes, you can change your exam location by submitting the form in
                                                        eServices > Exam Online. Please check the announcement in OUM
                                                        Portal for Examination Operational Date.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-3"
                                                   href="#collapse-3-23"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    23. Can I request for another copy of my transcript?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-3-23" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Yes, you can request through eCRM. To expedite the process, you
                                                        may contact us and schedule for collection of the transcripts at
                                                        1-300-887-300.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="faq4">
                    <div class="accordion-faq">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="underline">Rules and Regulations</div>
                                <div id="accordion-4" class="panel-group accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-1"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    1. I have completed a tertiary education programme before. Is there
                                                    any possibility to get my
                                                    credits transferred?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-1" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Yes, you may apply for credit transfer. However, approval is
                                                        subject to the University's credit transfer policy.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-2"
                                                   aria-expanded="true" class="accordion-toggle collapsed">
                                                    2. When can I apply for credit transfer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-2" style="" aria-expanded="true"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Application for Credit Transfer must be submitted in your first
                                                        semester
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading"><h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-3"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    3. How do I apply for credit transfer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-3" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Fill in the UKR 04 Form</li>
                                                    <li>Enclose a copy of the syllabus and results transcript.</li>
                                                    <li>Complete and submit the UKR 04 Form to:</li>
                                                    <br/>
                                                    <address class="text-primary">
                                                        <strong>REGISTRAR</strong><br/>
                                                        Open University,<br/>
                                                        Jalan Tun Ismail,<br/>
                                                        50480 Kuala Lumpur<br/>
                                                    </address>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-4"
                                                   aria-expanded="true" class="accordion-toggle collapsed">
                                                    4. Are there any charges involved in credit transfer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-4" style="" aria-expanded="true"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Yes, there is.
                                                        <ul>
                                                            <li>RM100.00 per subject for Diploma programmes</li>
                                                            <li>RM150.00 per subject for Degree programmes</li>
                                                            <li>RM300.00 per subject for Postgraduate programmes</li>
                                                        </ul>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-5"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    5. What is the maximum number of credits that can be transferred?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-5" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Not more than one-third of the total credit requirements.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-6"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    6. When will I be informed of the results of my application for
                                                    credit transfer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-6" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Within the first semester.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-7"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    7. What are the requirements for credit transfer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-7" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Undergraduate
                                                            </th>
                                                            <td class="text-primary">
                                                                <ul>
                                                                    <li>Learners with Diploma qualification pursuing a
                                                                        Bachelor degree at OUM. The Diploma must be
                                                                        accredited by MQA or at least obtained the
                                                                        minimum standard;
                                                                    </li>
                                                                    <li>The courses applied for the transfer of credits
                                                                        must be at par or the standard must even be
                                                                        higher than those offered by OUM;
                                                                    </li>
                                                                    <li>The contents of courses applied must be at least
                                                                        80% overlapping with those offered by OUM;
                                                                    </li>
                                                                    <li>The grade or grade point obtained for the
                                                                        courses applied must not be less than grade C or
                                                                        2.0 grade point;
                                                                    </li>
                                                                    <li>The number of credit-hours approved for the
                                                                        transfer of credits must not exceed one-third
                                                                        (1/3) of the total credit hours for a particular
                                                                        programme
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Postgraduate
                                                            </th>
                                                            <td class="text-primary">
                                                                <ul>
                                                                    <li>The courses are relevant to his/her programme of
                                                                        study;
                                                                    </li>
                                                                    <li>The credits to be exempted shall be for courses
                                                                        that are equivalent to those courses offered at
                                                                        the University and shall be of a minimum of
                                                                        grade B (65 marks and above);
                                                                    </li>
                                                                    <li>The courses applied must be taken not less than
                                                                        five (5) years prior to the application;
                                                                    </li>
                                                                    <li>The number of credit-hours approved for the
                                                                        transfer of credits must not exceed one-third
                                                                        (1/3) or 33% of the total credit hours for a
                                                                        particular programme.
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-8"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    8. What are the courses eligible for credit transfer?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-8" style="height: 0px;" aria-expanded="false"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        The list of courses is available in the student handbook of
                                                        which all registered students will be given access to.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-9"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    9. How do I register for the next semester?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-9" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Click on Registry Announcement to view any new information</li>
                                                    <li>Click at e-Services > UKR Online > Registration Online</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-10"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    10. How do I apply to change my programme of study?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-10" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Complete UKR 08 Form via online at e-Services > UKR Online >
                                                        Change Programme (UKR 08)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-11"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    11. How do I apply to change my Learning Centre?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-11" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li> Complete UKR 12 form via online at e-Services > UKR Online >
                                                        Change PPW (UKR 12)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-12"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    12. When can I ADD/DROP my course?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-12" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <tbody>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Add subject</th>
                                                            <td class="text-primary">Date of online registration until 3
                                                                days before date of online registration close
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" class="warning text-danger">Drop subject
                                                            </th>
                                                            <td class="text-primary">Date of online registration until
                                                                before T1
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <p class="text-warning">
                                                    <strong>
                                                        <i>The duration/dates to ADD/DROP subjects will be stated in the
                                                            Registry Announcement</i>
                                                    </strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-13"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    13. How do I apply to ADD/DROP my course?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-13" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Complete the UKR 06 form via online at e-Services > UKR Online
                                                        > Add/Drop Courses (UKR 06)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-14"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    14. When can I withdraw from taking my course?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-14" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>After T1 until 1 week before the examination date.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-15"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    15. How do I withdraw from my course?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-15" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Complete the UKR 07 form via online at e-Services > UKR Online >
                                                        Add/Drop Courses (UKR 07)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-16"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    16. When can I withdraw from my programme (Quit my studies)?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-16" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Anytime</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-17"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    17. How do I withdraw from my programme (Quit my studies) ?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-17" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Complete the UKR 11 form via online at e-Services > UKR Online
                                                        > Withdraw programme (Quit Studies) (UKR 11)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-18"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    18. When can I defer my studies ?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-18" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Anytime throughout the semester but 1 week before the
                                                        examination.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-19"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    19. How do I apply to defer my studies ?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-19" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Open market students - Complete the UKR 10 form via online at
                                                        e-Services > UKR Online > Deferment semester (UKR 10)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-20"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    20. When do I get my matric card ?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-20" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>From Tutorial 2 onwards</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-21"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    21. How long does it take to complete my studies ?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-21" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Diploma - Minima: 3 1/2 years</li>
                                                    <li>Bachelor - Minima: 4 1/2 years</li>
                                                    <li>Master – Minima: 2 years</li>
                                                </ul>
                                                <p class="text-warning">
                                                    Note: With credit transfer, learners may complete earlier than the
                                                    minimum duration specified above).
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-22"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    22. Can I change my learning centre ?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-22" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Yes, you can but you must apply in advance to change your
                                                        learning center
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-23"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    23. When can I receive my Degree Certificate and Academic
                                                    Transcript?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-23" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>You will receive the documents after the convocation ceremony
                                                        for your session has ended and your convocation robe has been
                                                        returned to the secretariat.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-4"
                                                   href="#collapse-4-24"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    24. Can I make any amendment or correction with regards to my
                                                    personal information in the certificate?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-4-24" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Before convocation:
                                                        <ul>
                                                            <li>
                                                                Any changes in name, address, telephone number and email
                                                                can be done in OUM Portal > myProfile > Edit > Save.
                                                            </li>
                                                            <li>
                                                                Other changes need to be submitted to Admission and
                                                                Records Unit via fax 03- 2697 8794. Please attach a copy
                                                                of your IC for our reference
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>After convocation:
                                                        <ul>
                                                            <li>
                                                                You need to contact the Assessment and Examination
                                                                Department at 03- 2773 2626.
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>Changes can be made for the following:
                                                        <ol>
                                                            <li>Scroll
                                                                <ol type="a">
                                                                    <li>Programme</li>
                                                                    <li>Name</li>
                                                                </ol>
                                                            </li>
                                                            <li> Transcript
                                                                <ol type="a">
                                                                    <li>Programme</li>
                                                                    <li>Name</li>
                                                                    <li>Major</li>
                                                                    <li>Grade</li>
                                                                    <li>GPA / CGPA</li>
                                                                    <li>Nationality</li>
                                                                    <li>Gender</li>
                                                                </ol>
                                                            </li>
                                                            <li>Others
                                                                <ol type="i">
                                                                    <li>Changes in spelling of name will be charged as
                                                                        follows:
                                                                        <ul>
                                                                            <li>Scroll: RM20</li>
                                                                            <li> Transcript: RM10</li>
                                                                            <li>Delivery charges: RM10</li>
                                                                        </ul>
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="faq5">
                    <div class="accordion-faq">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="underline">Teaching And Learning</div>
                                <div id="accordion-5" class="panel-group accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-1"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    1. Where and when are the tutorials/seminars conducted?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-1" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>At Learning Centres throughout the country.</li>
                                                    <li>On weekends (Saturday/Sunday) except Johor, Kelantan,
                                                        Terengganu & Kedah (Friday/Saturday)
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-2"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    2. When is the tutorial commencement?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-2" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Once a fortnight either :</li>
                                                    <ul>
                                                        <li>1st and 3rd weekend or;</li>
                                                        <li>2nd and 4th weekend as determined by the Learning Centre.
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-3"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    3. Who are the tutors / facilitators?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-3" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>OUM Tutors are specially appointed by the University based on
                                                        their relevant qualifications and experience.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-4"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    4. How many semesters are there in a year?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-4" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>There are 3 Semesters :</li>
                                                    <ul>
                                                        <li class="text-primary">January</li>
                                                        <li class="text-primary">May</li>
                                                        <li class="text-primary">September</li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-5"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    5. How many tutorial hours are allocated per course for every
                                                    semester?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-5" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-condensed">
                                                        <thead>
                                                        <tr class="warning text-danger">
                                                            <th>Level</th>
                                                            <th>No of tutorial class per semester</th>
                                                            <th>Tutorial/seminar hours per class</th>
                                                            <th>Tutorial/seminar hours per course</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="text-primary">
                                                            <th scope="row" class="danger text-warning">
                                                                Undergraduate<br/>*except Nursing
                                                            </th>
                                                            <td>4</td>
                                                            <td>2 Hours</td>
                                                            <td>8 Hours</td>
                                                        </tr>
                                                        <tr class="text-primary">
                                                            <th scope="row" class="danger text-warning">Nursing</th>
                                                            <td>5</td>
                                                            <td>2 Hours</td>
                                                            <td>10 Hours</td>
                                                        </tr>
                                                        <tr class="text-primary">
                                                            <th scope="row" class="danger text-warning">Postgraduate
                                                            </th>
                                                            <td>5</td>
                                                            <td>3 Hours</td>
                                                            <td>15 Hours</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-6"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    6. How do learners interact with their tutors?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-6" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Students will be provided with two types of tutors;</li>
                                                    <ul>
                                                        <li>Face-to-face tutor: conducts the tutorial class at the
                                                            learning center.
                                                        </li>
                                                        <li>eTutor: facilitates the online discussion in the OUM
                                                            Portal
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-7"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    7. Is it compulsory for students to attend tutorials/seminars?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-7" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Students are highly encouraged to attend the tutorials.
                                                        However, for Master of Counselling, seminar are compulsory for
                                                        student to attend
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-8"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    8. What are the activities conducted during tutorials?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-8" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Group discussions</li>
                                                    <li>Brainstorming session</li>
                                                    <li>Presentations</li>
                                                    <li>Mini lectures</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-9"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    9. How many learners are there per tutorial class?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-9" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Maximum of 30 learners</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-10"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    10. How do I study online?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-10" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Learners are provided access to OUM Portal (Learning Management
                                                        System).
                                                    </li>
                                                    <li>The portal is enriched with Learning Materials such as PDF
                                                        Module, Study Guide, links to E-Books, Video Lectures, Online
                                                        Discussions, etc for students’ references
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-11"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    11. What is the medium of instruction?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-11" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Both Bahasa Malaysia and English except for language courses
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-12"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    12. Who should I contact if I have problems regarding my tutor?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-12" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>You may contact Institute Teaching and Learning Advancement
                                                        via :
                                                    </li>
                                                    <ul class="text-primary">
                                                        <li>Email : <a href="mailto:itla@oum.edu.my" target="_blank">itla@oum.edu.my</a>
                                                        </li>
                                                        <li>Contact no:</li>
                                                        <ul>
                                                            <li>Face-to-Face (F2F) Tutoring Learning and Support (F2F
                                                                Tutor) : 03-2773 2301 / 2493
                                                            </li>
                                                            <li>Online Tutoring Learning and Support (E-tutor) : 03-2773
                                                                2226 / 2490
                                                            </li>
                                                        </ul>
                                                    </ul>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-13"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    13. Can I change my F2F Tutorial timetable?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-13" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>You may discuss with the Learning Centre Personnel for changing
                                                        of slot.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-14"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    14. Why am I not able to access my registered courses in
                                                    myINSPIRE to print my module although I have paid all the fees?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-14" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Your registered courses will be updated in myINSPIRE three (3)
                                                        working days after you have made the minimum payment of
                                                        registration for the Current Semester. For undergraduate
                                                        programs, the minimum fee is RM400 per semester.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-5"
                                                   href="#collapse-5-15"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    15. Why are my registered subjects not displayed in myINSPIRE?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-5-15" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>
                                                        Courses in myINSPIRE will be updated if the student has made a
                                                        complete registration, which means that students have submitted
                                                        their registration confirmation and have made the minimum
                                                        payment.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="faq6">
                    <div class="accordion-faq">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="underline">Technical And System Support</div>
                                <div id="accordion-6" class="panel-group accordion">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-6"
                                                   href="#collapse-6-1"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    1. Why am I not able to open my student e-mail account?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-6-1" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Please use the Google Chrome browser to access OUM Portal >
                                                        click on email icon. If you are still unsuccessful, please
                                                        submit screen shots of the problem to eCRM for assistance.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-6"
                                                   href="#collapse-6-2"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    2. Why are my registered subjects not displayed in myINSPIRE?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-6-2" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Courses in myINSPIRE will be updated if the student has made a
                                                        complete registration, which means that students have submitted
                                                        their registration confirmation and have made the minimum
                                                        payment.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-6"
                                                   href="#collapse-6-3"
                                                   aria-expanded="false" class="accordion-toggle collapsed">
                                                    3. Why am I not able to access my registered courses in myINSPIRE
                                                    to print my module although I have paid all the fees?
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse-6-3" aria-expanded="false" style="height: 0px;"
                                             class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="text-answer">
                                                    <li>Your registered courses will be updated in myINSPIRE three (3)
                                                        working days after you have made the minimum payment of
                                                        registration for the Current Semester. For undergraduate
                                                        programs, the minimum fee is RM400 per semester.
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>